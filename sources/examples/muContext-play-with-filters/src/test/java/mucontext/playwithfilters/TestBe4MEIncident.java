/**
This file is part of the muContext middleware.

Copyright (C) 2014-2017 Télécom SudParis

The INCOME platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The INCOME platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the INCOME platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */

package mucontext.playwithfilters;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.script.ScriptException;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import mucontext.datamodel.context.ContextDataModelFacade;
import mucontext.datamodel.context.ContextEntity;
import mucontext.datamodel.context.ContextObservable;
import mucontext.datamodel.context.ContextObservation;
import mucontext.datamodel.context.ContextReport;

import org.junit.Assert;
import org.junit.Test;
import org.xml.sax.SAXException;

public class TestBe4MEIncident {
	/**
	 * the filter to test.
	 */
	private String filterToTest = "function evaluate(doc) {"
			+ "load(\"nashorn:mozilla_compat.js\");"
			+ "importPackage(javax.xml.xpath);"
			+ "var xpath = XPathFactory.newInstance().newXPath();"
			+ "var n1 = xpath.evaluate('/contextReport', doc, XPathConstants.NODE);"
			+ "if (n1 == null) return false;"
			+ "var n2 = xpath.evaluate('//observable/name', n1, XPathConstants.NODE);"
			+ "if (!n2.getTextContent().equalsIgnoreCase('incident')) return false;"
			+ "var n3 = xpath.evaluate('//observation/value', n1, XPathConstants.NODE);"
			+ "if (n3 == null) return false;"
			+ "return n3.getTextContent().equalsIgnoreCase('ongoing');"
			+ "}";

	@Test
	public void testOngoingIncident() throws URISyntaxException, JAXBException,
			ScriptException, ParserConfigurationException, SAXException,
			IOException, XPathExpressionException {
		ContextDataModelFacade facade = new ContextDataModelFacade("facade");
		String uriPath = "mucontext://localhost:3005";
		String segment = "Caubiere-IUT";
		ContextEntity entity = facade.createContextEntity(segment, new URI(
				uriPath + "/bus/" + segment));
		ContextObservable observable = facade.createContextObservable(
				"incident", new URI(uriPath + "/bus/" + segment + "/incident"),
				entity);
		ContextObservation<?> observation1 = facade.createContextObservation(
				"o", "ongoing", observable);
		ContextReport report = facade.createContextReport("report");
		facade.addContextObservation(report, observation1);
		String serialised = facade.serialiseToXML(report);
		Assert.assertTrue(Util.evaluate(filterToTest, serialised));
	}

	@Test
	public void testNoIncident() throws URISyntaxException, JAXBException,
			ScriptException, ParserConfigurationException, SAXException,
			IOException, XPathExpressionException {
		ContextDataModelFacade facade = new ContextDataModelFacade("facade");
		String uriPath = "mucontext://localhost:3005";
		String segment = "Caubiere-IUT";
		ContextEntity entity = facade.createContextEntity(segment, new URI(
				uriPath + "/bus/" + segment));
		ContextObservable observable = facade.createContextObservable(
				"incident", new URI(uriPath + "/bus/" + segment + "/incident"),
				entity);
		ContextObservation<?> observation1 = facade.createContextObservation(
				"o", "none", observable);
		ContextReport report = facade.createContextReport("report");
		facade.addContextObservation(report, observation1);
		String serialised = facade.serialiseToXML(report);
		Assert.assertFalse(Util.evaluate(filterToTest, serialised));
	}
}

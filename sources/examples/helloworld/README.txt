This is the "hello world!" application of muContext.
So, this is where to start with muContext.

In this file:
-0- How to proceed when this is a snapshot release
-1- How to execute the "hello world!" application?
-2- How to create my new application entities from this example?
    -2.1- How to create a context collector?
    -2.2- How to create a context capsule?
    -2.3- How to create a context application?

0- How to proceed when this is a snapshot release

If the file pom.xml declares a snapshot release, then muContext must be
compiled and installed before compiling these examples.

$
(cd ../../muContext/; mvn clean && mvn -Djsse.enableSNIExtension=false install)
$

1- How to execute the "hello world!" application?

Check that muContext and the example are compiled and installed :
$
mvn clean install
$

In a first terminal, start BrokerA and CollectorHelloWorld
$
cd helloworld-collector
./run.sh
$

The context collector periodically pushes a context report containing a
context observation with the message "Hello World!".

In a second terminal, start CapsuleHelloWorld
$
cd helloworld-capsule
./run.sh
$

The context capsule consumes the context reports of the context
collector and pushes a context report with the value
"Hello World! transformed".

In a third terminal, start ApplicationHelloWorld
$
cd helloworld-application
./run.sh
$

The context application consumes the context reports of the context
capsule.

2- How to create my new application from this example?

2.1- How to create a context collector?

In a terminal, create an archetype from the context collector helloworld
$
cd helloworld-collector
mvn archetype:create-from-project -Darchetype.filteredExtensions=java
cd target/generated-sources/archetype/
mvn install
$

Create your new context collector
$
cd /tmp
mvn archetype:generate -DarchetypeCatalog=local
$
Choose the archetype you have just created
$
Choose archetype:
1: local -> archetype (archetype)
2: local -> income.application.helloworld:helloworld-collector-archetype (The context collector of helloworld.)
Choose a number or apply filter (format: [groupId:]artifactId, case sensitive contains): 2
$
Customise maven configuration (Be careful of the version number!)
$
Define value for groupId: : mydemo
Define value for artifactId: : mycollector
Define value for version:  1.0-SNAPSHOT: : 0.8-SNAPSHOT
Define value for package:  com.company: : mydemo.mycollector
$

Compile and install your new collector
$
cd mycollector
mvn install
$

Before executing the collector, replace the content of the variable
"CLIENT" in the script "run.sh" ("example.collector.HelloWorld") with
your new collector class "mydemo.mycollector.HelloWorld"
$
chmod a+x run.sh
./run.sh
$
  
2.2- How to create a context capsule?

Proceed as for the creation of a context collector in Section 2.1.

2.3- How to create a context application?

Proceed as for the creation of a context collector in Section 2.1.

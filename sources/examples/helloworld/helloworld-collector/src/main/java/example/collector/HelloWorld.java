/**
This file is part of the INCOME platform.

Copyright (C) 2014-2015 Telecom SudParis

The INCOME platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The INCOME platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the INCOME platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Xuan Li, Leon Lim
 */

package example.collector;

import java.net.URI;
import java.util.Vector;

import mucontext.api.ContextCollector;
import mucontext.contextcollector.BasicContextCollector;
import mucontext.datamodel.context.ContextDataModelFacade;
import mucontext.datamodel.context.ContextEntity;
import mucontext.datamodel.context.ContextObservable;
import mucontext.datamodel.context.ContextObservation;
import mucontext.datamodel.context.ContextReport;
import mucontext.datamodel.contextcontract.BasicContextProducerContract;
import mudebs.common.algorithms.OperationalMode;

/**
 * This class defines an example context collector.
 * 
 * @author Denis Conan
 * 
 */
public class HelloWorld {

	/**
	 * Number of iterations in the outer loop.
	 */
	private static final int NBITER_OUTER_LOOP = 1000;

	/**
	 * Time to wait during publications (in ms).
	 */
	private static final long TIME_TO_WAIT = 10000;

	/**
	 * the termination of the main thread is asked for.
	 */
	private static boolean termination = false;

	/**
	 * the advertisement filter.
	 */
	private static String advFilter = "function evaluate(doc) {"
			+ "load(\"nashorn:mozilla_compat.js\");" + ""
			+ "importPackage(javax.xml.xpath);"
			+ "var xpath = XPathFactory.newInstance().newXPath();"
			+ "var n1 = xpath.evaluate('//observable/name', doc, XPathConstants.NODE);"
			+ "if (!n1.getTextContent().equalsIgnoreCase('Something')) return false;"
			+ "xpath = XPathFactory.newInstance().newXPath();"
			+ "var n2 = xpath.evaluate('//entity/name', n1, XPathConstants.NODE);"
			+ "if (!n2.getTextContent().startsWith('Somebody')) return false;"
			+ "return true;}";

	/**
	 * the main method of the context collector.
	 * 
	 * @param args
	 *            The arguments of the context collector, which are seen as
	 *            command line arguments, that is to say as an array of strings.
	 * @throws Exception
	 *             Exception in main thread.
	 */
	public static void main(final String[] args) throws Exception {
		ContextCollector contextCollector = new BasicContextCollector(args);
		ContextDataModelFacade facade = new ContextDataModelFacade("facade");
		String uriPath = "mucontext://localhost:3000";
		ContextEntity userEntity = facade.createContextEntity("Somebody",
				new URI(uriPath + "/user/Somebody"));
		Vector<ContextEntity> entities = new Vector<ContextEntity>();
		entities.add(userEntity);
		ContextObservable observable = facade.createContextObservable(
				"Something", new URI(uriPath + "/Something"), userEntity);
		contextCollector.setContextProducerContract(
				new BasicContextProducerContract("somecontract",
						OperationalMode.LOCAL, advFilter, null));
		while (!termination) {
			for (int i = 0; i < NBITER_OUTER_LOOP; i++) {
				ContextObservation<?> observation1 = facade
						.createContextObservation("o" + i, "hello world!",
								observable);
				ContextReport report = facade.createContextReport("report" + i);
				facade.addContextObservation(report, observation1);
				contextCollector.push("somecontract",
						facade.serialiseToXML(report));
				System.out.println("Example context collector, " + "publishes"
						+ " 'report = " + report);
				Thread.sleep(TIME_TO_WAIT);
			}
			contextCollector.unsetAllContextProducerContracts();
			termination = true;
			contextCollector.shutdown();
		}
	}
}

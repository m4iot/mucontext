#!/bin/bash

MUCONTEXT_HOME_SCRIPTS="$(cd $(dirname "$0")/../../../muContext/scripts && pwd)"
. ${MUCONTEXT_HOME_SCRIPTS}/utils.sh

echo "Check that BrokerA, CollectorHelloWorld, and CapsuleHelloWorld are started"
echo "Hit return when you want to start ApplicationHelloWorld"
read x

CLIENT_ID=ApplicationHelloWorld
PORT_ID=2102
CLIENT="example.application.HelloWorld"

ARGS=" --uri mudebs://localhost:"${PORT_ID}"/"${CLIENT_ID}" --broker mudebs://localhost:2000/BrokerA" #--log COMM.TRACE

# if the JAVA_HOME is not set, set to the home directory of the jvm
if [ -z $JAVA_HOME ]
then
    # jvm is in $JAVA_HOME/jre/bin/; therefore three 'dirname'
    JAVA_HOME=$(dirname $(dirname $(dirname $(readlink -f '/usr/bin/java'))))
fi

# new values
MEMORY_MIN=16
MEMORY_MAX=64

CLASSPATH=$APPLICATION_ONE_CONSUMER_CLASSPATH${PATHSEP}"$(cd $(dirname "$0")/target/classes && pwd)"

JVM_ARGS="-Xms${MEMORY_MIN}m -Xmx${MEMORY_MAX}m -cp ${CLASSPATH}"

# start the client
CMD="java $JVM_ARGS $CLIENT ${ARGS}"

$CMD &

echo "Hit return when you want to stop the demonstration"
read x

# determine the PIDs of the run script and java jvm
SCRIPT_PID=$(ps aux | grep startclient | awk '/-uri mudebs:\/\/[A-Za-z0-9\.]+:[[:digit:]]+\/'$CLIENT_ID'($| )/{print $2}')
JAVA_PID=$(ps aux | grep java | awk '/-uri mudebs:\/\/[A-Za-z0-9\.]+:[[:digit:]]+\/'$CLIENT_ID'($| )/{print $2}')

# Find PIDs under cygwin
case "`uname`" in
    (CYGWIN*)
    # Reset pids in case we accidentally found the wrong ones above.
    SCRIPT_PID=""
    JAVA_PID="" 

    # Iterate through each process.
    for pid in $(ps -s |tail -n +2 |awk '{print $1}'); do
  	# Look for a process with the appropriate arguments
  	match=$([ -f /proc/$pid/cmdline ] && cat /proc/$pid/cmdline | xargs -0 | grep -w startclient | egrep -e "-uri mudebs://\w+:[[:digit:]]+/$CLIENT_ID")
  	if [ -n "$match" ]; then
  	    # Found a match
  	    JAVA_PID=$pid
  	    break # Only use the first match
  	fi
    done
    ;;
esac

# kill both processes; may not be necessary, just to be sure
killed=0
if [ ! -z $SCRIPT_PID ]; then
	kill -9 $SCRIPT_PID &> /dev/null
	killed=1
fi
if [ ! -z $JAVA_PID ]; then
	kill -9 $JAVA_PID &> /dev/null
	killed=1
fi

# print message
if [ $killed -eq 0 ]; then
    echo "The client $CLIENT_ID is not running"
else
    echo "Client $CLIENT_ID is stopped"
fi

/**
This file is part of the INCOME platform.

Copyright (C) 2013-2014 Telecom SudParis

The INCOME platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The INCOME platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the INCOME platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Xuan Li, Leon Lim
 */

package example.application;

import mucontext.api.ContextApplication;
import mucontext.api.ContextConsumer;
import mucontext.contextapplication.BasicContextApplicationOneConsumer;
import mucontext.datamodel.context.ContextDataModelFacade;
import mucontext.datamodel.context.ContextReport;
import mucontext.datamodel.contextcontract.BasicContextConsumerContract;
import mudebs.common.algorithms.OperationalMode;

/**
 * This class defines an example context application.
 * 
 * @author Denis Conan
 * 
 */
public class HelloWorld {

	/**
	 * The context application.
	 */
	private static ContextApplication contextApplication;

	/**
	 * Time to wait during polling (in ms).
	 */
	private static final long TIME_TO_WAIT = 5000;

	/**
	 * the termination of the main thread is asked for.
	 */
	private static boolean termination = false;

	/**
	 * Facade to the context data model.
	 */
	private static ContextDataModelFacade facade;

	/**
	 * the subscription filter.
	 */
	private static String subFilter = "function evaluate(doc) {"
			+ "load(\"nashorn:mozilla_compat.js\");"
			+ "importPackage(javax.xml.xpath);"
			+ "var xpath = XPathFactory.newInstance().newXPath();"
			+ "var n1 = xpath.evaluate('//observable/name', doc, XPathConstants.NODE);"
			+ "if (!n1.getTextContent().equalsIgnoreCase('SomethingElse')) return false;"
			+ "xpath = XPathFactory.newInstance().newXPath();"
			+ "var n2 = xpath.evaluate('//entity/name', n1, XPathConstants.NODE);"
			+ "if (!n2.getTextContent().equals('SomebodyElse')) return false;"
			+ "return true;}";

	private static ContextConsumer consumer = new ContextConsumer() {
		@Override
		public final void consume(final String contextData) {
			ContextReport report = facade.unserialiseFromXML(contextData);
			System.out.println("Example context application consumes report = "
					+ report);
		}
	};

	/**
	 * the main method of the application.
	 * 
	 * @param args
	 *            The arguments of the context application, which are seen as
	 *            command line arguments, that is to say as an array of strings.
	 * @throws Exception
	 *             Exception in main thread.
	 */
	public static void main(final String[] args) throws Exception {
		HelloWorld.contextApplication = new BasicContextApplicationOneConsumer(args);
		facade = new ContextDataModelFacade("facade");
		HelloWorld.contextApplication.setContextConsumerContract(
				new BasicContextConsumerContract("somecontract",
						OperationalMode.GLOBAL, subFilter, null), consumer);
		HelloWorld.contextApplication.setContextConsumerContract(
				new BasicContextConsumerContract("anothercontract",
						OperationalMode.GLOBAL, subFilter, null), consumer);
		while (!termination) {
			Thread.sleep(TIME_TO_WAIT);
		}
		HelloWorld.contextApplication.unsetAllContextConsumerContracts();
		HelloWorld.contextApplication.terminate();
	}

}

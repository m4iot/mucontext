/**
This file is part of the INCOME platform.

Copyright (C) 2014-2015 Telecom SudParis

The INCOME platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The INCOME platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the INCOME platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Xuan Li, Leon Lim
 */

package example.capsule;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Vector;

import mucontext.api.ContextCapsule;
import mucontext.api.ContextConsumer;
import mucontext.contextcapsule.BasicContextCapsule;
import mucontext.datamodel.context.ContextDataModelFacade;
import mucontext.datamodel.context.ContextEntity;
import mucontext.datamodel.context.ContextObservable;
import mucontext.datamodel.context.ContextObservation;
import mucontext.datamodel.context.ContextReport;
import mucontext.datamodel.contextcontract.BasicContextConsumerContract;
import mucontext.datamodel.contextcontract.BasicContextProducerContract;
import mudebs.common.MuDEBSException;
import mudebs.common.algorithms.OperationalMode;

/**
 * This class defines an example context capsule.
 * 
 * @author Denis Conan
 * 
 */
public class HelloWorld implements ContextConsumer {

	/**
	 * the reference to the context capsule for advertising, etc.
	 */
	private ContextCapsule capsule;

	/**
	 * Time to wait during polling (in ms).
	 */
	private static final long TIME_TO_WAIT = 5000;

	/**
	 * the termination of the main thread is asked for.
	 */
	private static boolean termination = false;

	/**
	 * Facade to the context data model.
	 */
	private static ContextDataModelFacade facade;

	/**
	 * the subscription filter.
	 */
	private static String subFilter = "function evaluate(doc) {"
			+ "load(\"nashorn:mozilla_compat.js\");"
                        + "importPackage(javax.xml.xpath);"
			+ "var xpath = XPathFactory.newInstance().newXPath();"
			+ "var n1 = xpath.evaluate('//observable/name', doc, XPathConstants.NODE);"
			+ "if (!n1.getTextContent().equalsIgnoreCase('Something')) return false;"
			+ "xpath = XPathFactory.newInstance().newXPath();"
			+ "var n2 = xpath.evaluate('//entity/name', n1, XPathConstants.NODE);"
			+ "if (!n2.getTextContent().equals('Somebody')) return false;"
			+ "return true;}";

	/**
	 * the advertisement filter.
	 */
	private static String advFilter = "function evaluate(doc) {"
			+ "load(\"nashorn:mozilla_compat.js\");"
			+ "importPackage(javax.xml.xpath);"
			+ "var xpath = XPathFactory.newInstance().newXPath();"
			+ "var n1 = xpath.evaluate('//observable/name', doc, XPathConstants.NODE);"
			+ "if (!n1.getTextContent().equalsIgnoreCase('SomethingElse')) return false;"
			+ "xpath = XPathFactory.newInstance().newXPath();"
			+ "var n2 = xpath.evaluate('//entity/name', n1, XPathConstants.NODE);"
			+ "if (!n2.getTextContent().equals('SomebodyElse')) return false;"
			+ "return true;}";

	/**
	 * processes the context data received.
	 * 
	 * @param contextData
	 *            The context data received
	 */
	public final void consume(final String contextData) {
		ContextReport report = facade.unserialiseFromXML(contextData);
		System.out.println("Example context capsule consumes report = "
				+ report);
		ContextObservation<?> observation = report
				.observationMatchObservable("Something");
		String uriPath = "mucontext://localhost:3000";
		ContextEntity userEntity;
		try {
			userEntity = facade.createContextEntity("SomebodyElse", new URI(
					uriPath + "/user/SomebodyElse"));
		} catch (URISyntaxException e) {
			e.printStackTrace();
			return;
		}
		Vector<ContextEntity> entities = new Vector<ContextEntity>();
		entities.add(userEntity);
		ContextObservable observable;
		try {
			observable = facade.createContextObservable("SomethingElse",
					new URI(uriPath + "/SomethingElse"), userEntity);
		} catch (URISyntaxException e) {
			e.printStackTrace();
			return;
		}
		ContextObservation<?> observation1 = facade.createContextObservation(
				"o", observation.value + " transformed", observable);
		report = facade.createContextReport("report");
		facade.addContextObservation(report, observation1);
		try {
			capsule.push("someproducercontract", facade.serialiseToXML(report));
		} catch (MuDEBSException e) {
			e.printStackTrace();
			return;
		}
		System.out.println("Example context capsule publishes report = "
				+ report);
	}

	/**
	 * Main method of the TagUrLocation context capsule.
	 * 
	 * @param args
	 *            The arguments of the TagUrLocation context capsule, which are
	 *            seen as command line arguments, that is to say as an array of
	 *            strings.
	 * @throws Exception
	 *             Exception in main thread.
	 */
	public static void main(final String[] args) throws Exception {
		HelloWorld me = new HelloWorld();
		me.capsule = new BasicContextCapsule(args);
		facade = new ContextDataModelFacade("facade");
		me.capsule.setContextConsumerContract(
				new BasicContextConsumerContract("someconsumercontract",
						OperationalMode.GLOBAL, subFilter, null),
				(ContextConsumer) me);
		me.capsule
				.setContextProducerContract(new BasicContextProducerContract(
						"someproducercontract", OperationalMode.LOCAL,
						advFilter, null));
		while (!termination) {
			Thread.sleep(TIME_TO_WAIT);
		}
		me.capsule.unsetAllContextProducerContracts();
		me.capsule.terminate();
	}
}

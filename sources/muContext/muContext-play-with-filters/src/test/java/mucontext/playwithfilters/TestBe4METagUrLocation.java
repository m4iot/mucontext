/**
This file is part of the muContext middleware.

Copyright (C) 2014-2017 Télécom SudParis

The INCOME platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The INCOME platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the INCOME platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */

package mucontext.playwithfilters;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Vector;

import javax.script.ScriptException;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import mucontext.datamodel.context.ContextDataModelFacade;
import mucontext.datamodel.context.ContextEntity;
import mucontext.datamodel.context.ContextObservable;
import mucontext.datamodel.context.ContextObservation;
import mucontext.datamodel.context.ContextReport;
import mucontext.datamodel.context.EntityRelationType;

import org.junit.Assert;
import org.junit.Test;
import org.xml.sax.SAXException;

public class TestBe4METagUrLocation {
	/**
	 * the first filter to test.
	 */
	private String filterToTest1 = "function evaluate(doc) {"
			+ "load(\"nashorn:mozilla_compat.js\");"
			+ "importPackage(javax.xml.xpath);"
			+ "var xpath = XPathFactory.newInstance().newXPath();"
			+ "var n1 = xpath.evaluate('//observable/name', doc, XPathConstants.NODE);"
			+ "if (n1 == null) return false;"
			+ "if (!n1.getTextContent().equalsIgnoreCase('presencebusrfidtag')) return false;"
			+ "xpath = XPathFactory.newInstance().newXPath();"
			+ "var n2 = xpath.evaluate('//entity/name', n1, XPathConstants.NODE);"
			+ "if (n2 == null) return false;"
			+ "if (!n2.getTextContent().startsWith('rfidtagbus')) return false;"
			+ "xpath = XPathFactory.newInstance().newXPath();"
			+ "var n3 = xpath.evaluate('//relation/name', n2, XPathConstants.NODE);"
			+ "if (n3 == null) return false;"
			+ "return n3.getTextContent().equalsIgnoreCase('inbus');}";
	/**
	 * the second filter to test.
	 */
	private String filterToTest2 = "function evaluate(doc) {"
			+ "load(\"nashorn:mozilla_compat.js\");"
			+ "importPackage(javax.xml.xpath);"
			+ "var xpath = XPathFactory.newInstance().newXPath();"
			+ "var n1 = xpath.evaluate('//observable/name', doc, XPathConstants.NODE);"
			+ "if (!n1.getTextContent().equalsIgnoreCase('locationItinerary')) return false;"
			+ "xpath = XPathFactory.newInstance().newXPath();"
			+ "var n2 = xpath.evaluate('//relation/name', n1, XPathConstants.NODE);"
			+ "return n2.getTextContent().equalsIgnoreCase('locatedat');}";

	@Test
	public void testFirstFilter() throws URISyntaxException, JAXBException,
			ScriptException, ParserConfigurationException, SAXException,
			IOException, XPathExpressionException {
		ContextDataModelFacade facade = new ContextDataModelFacade("facade");
		String uriPath = "mucontext://localhost:3000";
		ContextEntity userEntity = facade.createContextEntity("Leon",
				new URI(uriPath + "/user/Leon"));
		ContextEntity busEntity = facade.createContextEntity("rfidtagbus367",
				new URI(uriPath + "/rfidtag/bus367"));
		EntityRelationType inbustype = facade
				.createEntityRelationType("inbustype");
		Vector<ContextEntity> entities = new Vector<ContextEntity>();
		entities.add(userEntity);
		entities.add(busEntity);
		facade.createBinaryEntityRelation("inbus", inbustype, entities);
		ContextObservable observable = facade.createContextObservable(
				"presencebusrfidtag",
				new URI(uriPath + "/presence/bus367/rfidtag"), busEntity);
		ContextObservation<?> observation1 = facade
				.createContextObservation("o", new Integer(1), observable);
		ContextReport report = facade.createContextReport("report");
		facade.addContextObservation(report, observation1);
		String serialised = facade.serialiseToXML(report);
		Assert.assertTrue(Util.evaluate(filterToTest1, serialised));
	}

	@Test
	public void testSecondFilter() throws URISyntaxException, JAXBException,
			ScriptException, ParserConfigurationException, SAXException,
			IOException, XPathExpressionException {
		String uriPath = "mucontext://localhost:3001";
		String userName = "Chantal";
		String busLine = "2";
		String busNumber = "327";
		ContextDataModelFacade facade = new ContextDataModelFacade("facade");
		ContextEntity userEntity = facade.createContextEntity(userName,
				new URI(uriPath + "/user/" + userName));
		ContextEntity busLineEntity = facade.createContextEntity(busLine,
				new URI(uriPath + "/bus/line/" + busNumber
						+ "/UniversitéPaulSabatier"));
		EntityRelationType inbustype = facade
				.createEntityRelationType("locatedattype");
		Vector<ContextEntity> entities = new Vector<ContextEntity>();
		entities.add(userEntity);
		entities.add(busLineEntity);
		facade.createBinaryEntityRelation("locatedat", inbustype, entities);
		ContextObservable observable = facade.createContextObservable(
				"locationItinerary", new URI(uriPath + "/location/busline/"
						+ busNumber + "/UniversitéPaulSabatier"),
				busLineEntity);
		ContextReport report = null;
		report = facade.createContextReport("report");
		ContextObservation<?> toPublish = facade.createContextObservation("o",
				"Pastourelles", observable);
		facade.addContextObservation(report, toPublish);
		String serialised = facade.serialiseToXML(report);
		Assert.assertTrue(Util.evaluate(filterToTest2, serialised));
	}
}

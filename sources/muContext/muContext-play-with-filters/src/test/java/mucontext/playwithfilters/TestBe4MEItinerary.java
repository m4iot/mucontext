/**
This file is part of the muContext middleware.

Copyright (C) 2014-2017 Télécom SudParis

The INCOME platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The INCOME platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the INCOME platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */

package mucontext.playwithfilters;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.script.ScriptException;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import mucontext.datamodel.context.ContextDataModelFacade;
import mucontext.datamodel.context.ContextEntity;
import mucontext.datamodel.context.ContextObservable;
import mucontext.datamodel.context.ContextObservation;
import mucontext.datamodel.context.ContextReport;
import mucontext.datamodel.feedback.Feedback;
import mucontext.datamodel.feedback.FeedbackDataModelFacade;
import mucontext.datamodel.feedback.FeedbackReport;
import mucontext.datamodel.recommendation.Cause;
import mucontext.datamodel.recommendation.Proposition;
import mucontext.datamodel.recommendation.Recommendation;
import mucontext.datamodel.recommendation.RecommendationDataModelFacade;
import mucontext.datamodel.recommendation.RecommendationReport;

import org.junit.Assert;
import org.junit.Test;
import org.xml.sax.SAXException;

public class TestBe4MEItinerary {
	/**
	 * the first filter to test.
	 */
	private String filterToTest1 = "function evaluate(doc) {"
			+ "load(\"nashorn:mozilla_compat.js\");"
			+ "importPackage(javax.xml.xpath);"
			+ "var xpath = XPathFactory.newInstance().newXPath();"
			+ "var n1 = xpath.evaluate('/contextReport', doc, XPathConstants.NODE);"
			+ "if (n1 == null) return false;"
			+ "xpath = XPathFactory.newInstance().newXPath();"
			+ "var n2 = xpath.evaluate('//observable/name', doc, XPathConstants.NODE);"
			+ "return n2.getTextContent().equalsIgnoreCase('incident');}";

	/**
	 * the second filter to test.
	 */
	private String filterToTest2 = "function evaluate(doc) {"
			+ "load(\"nashorn:mozilla_compat.js\");"
			+ "importPackage(javax.xml.xpath);"
			+ "var xpath = XPathFactory.newInstance().newXPath();"
			+ "var n1 = xpath.evaluate('/feedbackReport', doc, XPathConstants.NODE);"
			+ "return (n1 != null);}";

	/**
	 * the third filter to test.
	 */
	private String filterToTest3 = "function evaluate(doc) {"
			+ "load(\"nashorn:mozilla_compat.js\");"
			+ "importPackage(javax.xml.xpath);"
			+ "var xpath = XPathFactory.newInstance().newXPath();"
			+ "var n1 = xpath.evaluate('/recommendationReport', doc, XPathConstants.NODE);"
			+ "return (n1 != null);}";

	@Test
	public void testFirstFilter() throws URISyntaxException, JAXBException,
			ScriptException, ParserConfigurationException, SAXException,
			IOException, XPathExpressionException {
		ContextDataModelFacade facade = new ContextDataModelFacade("facade");
		String uriPath = "mucontext://localhost:3005";
		String segment = "Caubiere-IUT";
		ContextEntity entity = facade.createContextEntity(segment, new URI(
				uriPath + "/bus/" + segment));
		ContextObservable observable = facade.createContextObservable(
				"incident", new URI(uriPath + "/bus/" + segment + "/incident"),
				entity);
		ContextObservation<?> observation1 = facade.createContextObservation(
				"o", "ongoing", observable);
		ContextReport report = facade.createContextReport("report");
		facade.addContextObservation(report, observation1);
		String serialised = facade.serialiseToXML(report);
		Assert.assertTrue(Util.evaluate(filterToTest1, serialised));
	}

	@Test
	public void testSecondFilter() throws URISyntaxException, JAXBException,
			ScriptException, ParserConfigurationException, SAXException,
			IOException, XPathExpressionException {
		FeedbackDataModelFacade facadeFeedback = new FeedbackDataModelFacade(
				"facadeFeedBack");
		RecommendationDataModelFacade facadeRecommendation = new RecommendationDataModelFacade(
				"facade");
		ContextDataModelFacade facadeContext = new ContextDataModelFacade(
				"facadeContext");
		ContextEntity entity = facadeContext.createContextEntity("Sophie",
				new URI("mucontext://localhost:3000/user/Sophie"));
		Proposition<?> proposition = facadeRecommendation.createProposition(
				"Itinerary", new Integer(1));
		Feedback feedback = facadeFeedback.createFeedback("itineraryFeedback",
				proposition, entity);
		FeedbackReport report = facadeFeedback
				.createFeedbackReport("feedbackReport");
		facadeFeedback.addFeedback(report, feedback);
		String serialised = facadeFeedback.serialiseToXML(report);
		Assert.assertTrue(Util.evaluate(filterToTest2, serialised));
	}

	@Test
	public void testThirdFilter() throws URISyntaxException, JAXBException,
			ScriptException, ParserConfigurationException, SAXException,
			IOException, XPathExpressionException {
		ContextDataModelFacade facadeC = new ContextDataModelFacade("facade");
		String uriPath = "mucontext://localhost:3005";
		String segment = "Caubiere-IUT";
		ContextEntity entity = facadeC.createContextEntity(segment, new URI(
				uriPath + "/bus/" + segment));
		ContextObservable observable = facadeC.createContextObservable(
				"incident", new URI(uriPath + "/bus/" + segment + "/incident"),
				entity);
		ContextObservation<?> observation1 = facadeC.createContextObservation(
				"o", "ongoing", observable);
		ContextReport creport = facadeC.createContextReport("report");
		facadeC.addContextObservation(creport, observation1);
		RecommendationDataModelFacade facade = new RecommendationDataModelFacade(
				"facadeRecommendation");
		Cause cause = facade.createCause("incident", creport);
		Recommendation recommendation = facade
				.createRecommendation("alternativeItineraries");
		facade.addCause(recommendation, cause);
		facade.addProposition(recommendation,
				facade.createProposition("Itinerary", new Integer(1)));
		RecommendationReport report = facade
				.createRecommendationReport("recommendationReport");
		facade.addRecommendation(report, recommendation);
		String serialised = facade.serialiseToXML(report);
		Assert.assertTrue(Util.evaluate(filterToTest3, serialised));
	}
}

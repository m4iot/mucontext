/**
This file is part of the muContext middleware.

Copyright (C) 2014-2017 Télécom SudParis

The INCOME platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The INCOME platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the INCOME platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */

package mucontext.playwithfilters;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Vector;

import javax.script.ScriptException;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import mucontext.datamodel.context.ContextDataModelFacade;
import mucontext.datamodel.context.ContextEntity;
import mucontext.datamodel.context.ContextObservable;
import mucontext.datamodel.context.ContextObservation;
import mucontext.datamodel.context.ContextReport;
import mucontext.datamodel.context.EntityRelationType;
import mucontext.datamodel.multiscalability.Dimension;
import mucontext.datamodel.multiscalability.EventDistributionRange;
import mucontext.datamodel.multiscalability.Measure;
import mucontext.datamodel.multiscalability.MultiscalabilityDataModelFacade;
import mucontext.datamodel.multiscalability.MultiscalabilityReport;
import mucontext.datamodel.multiscalability.ProjectionValue;
import mucontext.datamodel.multiscalability.Scale;
import mucontext.datamodel.multiscalability.ScaleSet;
import mucontext.datamodel.multiscalability.Viewpoint;

import org.junit.Assert;
import org.junit.Test;
import org.xml.sax.SAXException;

public class TestBe4MEDistUrLocation {
	/**
	 * the subscription and advertisement filter.
	 */
	private static String filterToTest = "function evaluate(doc) {"
			+ "load(\"nashorn:mozilla_compat.js\");"
			+ "importPackage(javax.xml.xpath);"
			+ "var xpath = XPathFactory.newInstance().newXPath();"
			+ "var n1 = xpath.evaluate('//observable/name', doc, XPathConstants.NODE);"
			+ "if (n1 == null) return false;"
			+ "if (!n1.getTextContent().equalsIgnoreCase('presencebusrfidtag')) return false;"
			+ "xpath = XPathFactory.newInstance().newXPath();"
			+ "var n2 = xpath.evaluate('//entity/name', n1, XPathConstants.NODE);"
			+ "if (n2 == null) return null;"
			+ "if (!n2.getTextContent().startsWith('rfidtagbus')) return false;"
			+ "xpath = XPathFactory.newInstance().newXPath();"
			+ "var n3 = xpath.evaluate('//scale/name', doc, XPathConstants.NODE);"
			+ "if (n3 == null) return false;"
			+ "if (!n3.getTextContent().equalsIgnoreCase('Universe encompassing all the worlds')) return false;"
			+ "xpath = XPathFactory.newInstance().newXPath();"
			+ "var n4 = xpath.evaluate('//measure/value', n3, XPathConstants.NODE);"
			+ "if (n4 == null) return false;"
			+ "if (!n4.getTextContent().equalsIgnoreCase('UNIVERSE')) return false;" + "return true;}";

	@Test
	public void testWithUniverse() throws URISyntaxException, JAXBException, ScriptException,
			ParserConfigurationException, SAXException, IOException, XPathExpressionException {
		ContextDataModelFacade facade = new ContextDataModelFacade("facade");
		String uriPath = "mucontext://localhost:3000";
		ContextEntity userEntity = facade.createContextEntity("Leon", new URI(uriPath + "/user/Leon"));
		ContextEntity busEntity = facade.createContextEntity("rfidtagbus367", new URI(uriPath
				+ "/rfidtag/bus367"));
		EntityRelationType inbustype = facade.createEntityRelationType("inbustype");
		Vector<ContextEntity> entities = new Vector<ContextEntity>();
		entities.add(userEntity);
		entities.add(busEntity);
		facade.createBinaryEntityRelation("inbus", inbustype, entities);
		MultiscalabilityDataModelFacade mfacade = new MultiscalabilityDataModelFacade("facade");
		Viewpoint viewpoint = mfacade.CreateViewpoint("Distributed" + " Event-Based Systems (DEBS)");
		Dimension dimension = mfacade.createDimension("Event Distribution Range (EDR)", viewpoint);
		Measure<?> measure = mfacade.createMeasure("EDR measure (EDRM)", EventDistributionRange.UNIVERSE);
		ScaleSet scaleset = mfacade.createScaleSet("EDR" + " in EDRM scaleset", dimension, measure);
		Scale scale = mfacade.createScale("Universe encompassing all the worlds", scaleset);
		ProjectionValue projectionvalue = mfacade.createProjectionValue("pv", scale);
		MultiscalabilityReport mr = mfacade.createMultiscalabilityReport("mreport");
		mfacade.addProjectionValue(mr, projectionvalue);
		ContextObservable observable = facade.createContextObservable("presencebusrfidtag", new URI(uriPath
				+ "/presence/bus367/rfidtag"), busEntity);
		ContextObservation<?> observation1 = facade.createContextObservation("o", new Integer(1), observable);
		ContextReport report = facade.createContextReport("report");
		facade.addMultiscalabilityReport(report, mr);
		facade.addContextObservation(report, observation1);
		String serialised = facade.serialiseToXML(report);
		Assert.assertTrue(Util.evaluate(filterToTest, serialised));
	}

	@Test
	public void testWithWorld() throws URISyntaxException, JAXBException, ScriptException,
			ParserConfigurationException, SAXException, IOException, XPathExpressionException {
		ContextDataModelFacade facade = new ContextDataModelFacade("facade");
		String uriPath = "mucontext://localhost:3000";
		ContextEntity userEntity = facade.createContextEntity("Leon", new URI(uriPath + "/user/Leon"));
		ContextEntity busEntity = facade.createContextEntity("rfidtagbus367", new URI(uriPath
				+ "/rfidtag/bus367"));
		EntityRelationType inbustype = facade.createEntityRelationType("inbustype");
		Vector<ContextEntity> entities = new Vector<ContextEntity>();
		entities.add(userEntity);
		entities.add(busEntity);
		facade.createBinaryEntityRelation("inbus", inbustype, entities);
		MultiscalabilityDataModelFacade mfacade = new MultiscalabilityDataModelFacade("facade");
		Viewpoint viewpoint = mfacade.CreateViewpoint("Distributed" + " Event-Based Systems (DEBS)");
		Dimension dimension = mfacade.createDimension("Event Distribution Range (EDR)", viewpoint);
		Measure<?> measure = mfacade.createMeasure("EDR measure (EDRM)", EventDistributionRange.WORLD);
		ScaleSet scaleset = mfacade.createScaleSet("EDR" + " in EDRM scaleset", dimension, measure);
		Scale scale = mfacade.createScale("Local DEBS world", scaleset);
		ProjectionValue projectionvalue = mfacade.createProjectionValue("pv", scale);
		MultiscalabilityReport mr = mfacade.createMultiscalabilityReport("mreport");
		mfacade.addProjectionValue(mr, projectionvalue);
		ContextObservable observable = facade.createContextObservable("presencebusrfidtag", new URI(uriPath
				+ "/presence/bus367/rfidtag"), busEntity);
		ContextObservation<?> observation1 = facade.createContextObservation("o", new Integer(1), observable);
		ContextReport report = facade.createContextReport("report");
		facade.addMultiscalabilityReport(report, mr);
		facade.addContextObservation(report, observation1);
		String serialised = facade.serialiseToXML(report);
		Assert.assertFalse(Util.evaluate(filterToTest, serialised));
	}
}

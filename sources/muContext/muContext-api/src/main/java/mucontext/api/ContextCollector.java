/**
This file is part of the muContext middleware.

Copyright (C) 2014-2017 Télécom SudParis

The INCOME platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The INCOME platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the INCOME platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Xuan Li, Léon Lim
 */

package mucontext.api;

import javax.xml.transform.TransformerException;

import mudebs.common.MuDEBSException;
import mudebs.common.algorithms.ACK;
import mudebs.common.algorithms.CommandResult;

/**
 * This interface defines a generic context collector, which sets and resets
 * context producer contracts, and pushes context data. Every call is duplicated
 * to provide a blocking and a non-blocking version. Please refer to the
 * documentation of the class <tt>ACK</tt> for the categories of
 * acknowledgements and their meaning.
 * 
 * @author Denis Conan
 * @author Xuan Li
 * @author Léon Lim
 * 
 */
public interface ContextCollector {

	/**
	 * Sets a context producer contract. This call is blocking when setting the
	 * argument <tt>ack</tt> to <tt>ACK.{LOCAL|GLOBAL}</tt>.
	 * 
	 * @param contract
	 *            the context producer contract.
	 * @param ack
	 *            the acknowledgement requirement.
	 * @return the result of the command.
	 * @throws MuDEBSException
	 *             the exception thrown by muDEBS.
	 * @throws TransformerException
	 *             the exception thrown by transformer.
	 */
	CommandResult setContextProducerContract(ContextProducerContract contract,
			ACK ack) throws MuDEBSException, TransformerException;

	/**
	 * Sets a context producer contract. This call is non-blocking and no
	 * acknowledgement is waited for.
	 * 
	 * @param contract
	 *            the context producer contract.
	 * @throws MuDEBSException
	 *             the exception thrown by muDEBS.
	 * @throws TransformerException
	 *             the exception thrown by the JavaScript engine.
	 */
	void setContextProducerContract(ContextProducerContract contract)
			throws MuDEBSException, TransformerException;

	/**
	 * Unsets a context producer contract. This call is blocking when setting
	 * the argument <tt>ack</tt> to <tt>ACK.{LOCAL|GLOBAL}</tt>.
	 * 
	 * @param identifier
	 *            the identifier of the contract to unregister.
	 * @param ack
	 *            the acknowledgement requirement.
	 * @return the result of the command.
	 * @throws MuDEBSException
	 *             the exception thrown by muDEBS.
	 */
	CommandResult unsetContextProducerContract(String identifier, ACK ack)
			throws MuDEBSException;

	/**
	 * Unsets a context producer contract. This call is non-blocking and no
	 * acknowledgement is waited for.
	 * 
	 * @param identifier
	 *            the identifier of the contract to unregister.
	 * @throws MuDEBSException
	 *             the exception thrown by muDEBS.
	 */
	void unsetContextProducerContract(String identifier) throws MuDEBSException;

	/**
	 * Resets all the context producer contracts. This call is blocking when
	 * setting the argument <tt>ack</tt> to <tt>ACK.{LOCAL|GLOBAL}</tt>.
	 * 
	 * @param ack
	 *            the acknowledgement requirement.
	 * @return the result of the command.
	 * @throws MuDEBSException
	 *             the exception thrown by muDEBS.
	 */
	CommandResult unsetAllContextProducerContracts(ACK ack)
			throws MuDEBSException;

	/**
	 * Resets all the context producer contracts. This call is non-blocking and
	 * no acknowledgement is waited for.
	 * 
	 * @throws MuDEBSException
	 *             the exception thrown by muDEBS.
	 */
	void unsetAllContextProducerContracts() throws MuDEBSException;

	/**
	 * Pushes context data. This call is blocking when setting the argument
	 * <tt>ack</tt> to <tt>ACK.LOCAL</tt>. Note that by a design choice
	 * <tt>ACK.GLOBAL</tt> is not allowed and leads to the same effect as
	 * <tt>ACK.LOCAL</tt>.
	 * 
	 * @param identifier
	 *            the identifier of the context producer contract.
	 * @param contextData
	 *            the context data to push.
	 * @param ack
	 *            the acknowledgement requirement.
	 * @return the result of the command.
	 * @throws MuDEBSException
	 *             the exception thrown by muDEBS.
	 */
	CommandResult push(String identifier, String contextData, ACK ack)
			throws MuDEBSException;

	/**
	 * Pushes context data. This call is non-blocking and no acknowledgement is
	 * waited for.
	 * 
	 * @param identifier
	 *            the identifier of the context producer contract.
	 * @param contextData
	 *            the context data to push.
	 * @throws MuDEBSException
	 *             the exception thrown by muDEBS.
	 */
	void push(String identifier, String contextData) throws MuDEBSException;

	/**
	 * Shuts down the context collector.
	 */
	void shutdown();
}

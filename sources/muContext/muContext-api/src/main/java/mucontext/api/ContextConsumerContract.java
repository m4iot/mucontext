/**
This file is part of the muContext middleware.

Copyright (C) 2014-2017 Télécom SudParis

The INCOME platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The INCOME platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the INCOME platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */

package mucontext.api;

import mudebs.common.algorithms.routing.ABACInformation;

/**
 * This interface defines a generic context consumer contract.
 * 
 * The contract may be local or global. In other words, since a consumer
 * contract leads to a muDEBS subscription, the subscription may be local or
 * global. More precisely, a local subscription is deployed only in the access
 * broker of the consumer. This implies that the publications containing context
 * data must be transported to the access broker of the consumer before being
 * selected for notification to the consumer. A global subscription is deployed
 * in all the brokers and a distributed routing algorithm implement the
 * transportation of the context data from remote producers to the consumer by
 * using the deployed subscription.
 * 
 * @author Denis Conan
 */
public interface ContextConsumerContract extends ContextContract {
	/**
	 * get the ABAC information of the context contract.
	 * 
	 * @return the ABAC information (provided by the consumer) used by the XACML
	 *         policy (provided by the producer).
	 */
	ABACInformation abacInformation();
}

/**
This file is part of the muContext middleware.

Copyright (C) 2014-2017 Télécom SudParis

The INCOME platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The INCOME platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the INCOME platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Xuan Li, Léon Lim
 */

package mucontext.api;

import java.util.List;

import javax.xml.transform.TransformerException;

import mudebs.client.api.Consumer;
import mudebs.common.MuDEBSException;
import mudebs.common.algorithms.ACK;
import mudebs.common.algorithms.CommandResult;
import mudebs.common.algorithms.Reply;
import mudebs.common.algorithms.routing.ABACInformation;
import mudebs.common.algorithms.routing.PublicationMsgContent;

/**
 * This interface defines a generic context capsule, which sets and resets
 * context producer and context consumer contracts, and pushes and pulls context
 * data. Every call is duplicated to provide a blocking and a non-blocking
 * version. Please refer to the documentation of the class <tt>ACK</tt> for the
 * categories of acknowledgements and their meaning.
 * 
 * @author Denis Conan
 * @author Xuan Li
 * @author Léon Lim
 * 
 */
public interface ContextCapsule extends Consumer {

	/**
	 * sets a context producer contract. This call is blocking when setting the
	 * argument <tt>ack</tt> to <tt>ACK.{LOCAL|GLOBAL}</tt>.
	 * 
	 * @param contract
	 *            the context producer contract.
	 * @param ack
	 *            the boolean stating whether an acknowledgement is requirement.
	 * @return the result of the command.
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem.
	 * @throws TransformerException
	 *             the exception thrown by the transformer.
	 */
	CommandResult setContextProducerContract(ContextProducerContract contract,
			ACK ack) throws MuDEBSException, TransformerException;

	/**
	 * sets a context producer contract. This call is non-blocking and no
	 * acknowledgement is waited for.
	 * 
	 * @param contract
	 *            the context producer contract.
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem.
	 * @throws TransformerException
	 *             the exception thrown by the transformer
	 */
	void setContextProducerContract(ContextProducerContract contract)
			throws MuDEBSException, TransformerException;

	/**
	 * unsets a context producer contract. This call is blocking when setting
	 * the argument <tt>ack</tt> to <tt>ACK.{LOCAL|GLOBAL}</tt>.
	 * 
	 * @param identifier
	 *            the identifier of the contract to unregister.
	 * @param ack
	 *            the boolean stating whether an acknowledgement is expected.
	 * @return the result of the command.
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem.
	 */
	CommandResult unsetContextProducerContract(String identifier, ACK ack)
			throws MuDEBSException;

	/**
	 * unsets a context producer contract. This call is non-blocking and no
	 * acknowledgement is waited for.
	 * 
	 * @param identifier
	 *            the identifier of the contract to unregister.
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem.
	 */
	void unsetContextProducerContract(String identifier) throws MuDEBSException;

	/**
	 * resets all the context producer contracts. This call is blocking when
	 * setting the argument <tt>ack</tt> to <tt>ACK.{LOCAL|GLOBAL}</tt>.
	 * 
	 * @param ack
	 *            the boolean stating whether an acknowledgement is expected.
	 * @return the result of the command.
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem.
	 */
	CommandResult unsetAllContextProducerContracts(ACK ack)
			throws MuDEBSException;

	/**
	 * resets all the context producer contracts. This call is non-blocking and
	 * no acknowledgement is waited for.
	 * 
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem.
	 */
	void unsetAllContextProducerContracts() throws MuDEBSException;

	/**
	 * pushes context data. This call is blocking when setting the argument
	 * <tt>ack</tt> to <tt>ACK.LOCAL</tt>. Following a design choice, there is
	 * no global acknowledgement when pushing context data: <tt>ACK.GLOBAL</tt>
	 * leads to the same effect as <tt>ACK.LOCAL</tt>.
	 * 
	 * @param identifier
	 *            the identifier of the contract.
	 * @param contextData
	 *            The context data to push.
	 * @param ack
	 *            the boolean stating whether an acknowledgement is expected.
	 * @return the result of the command.
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem.
	 */
	CommandResult push(String identifier, String contextData, ACK ack)
			throws MuDEBSException;

	/**
	 * pushes context data. This call is non-blocking and no acknowledgement is
	 * waited for. This call is non-blocking and no acknowledgement is waited
	 * for.
	 * 
	 * @param identifier
	 *            the identifier of the contract.
	 * @param contextData
	 *            The context data to push.
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem.
	 */
	void push(String identifier, String contextData) throws MuDEBSException;

	/**
	 * sets a context consumer contract. This call is blocking when setting the
	 * argument <tt>ack</tt> to <tt>ACK.{LOCAL|GLOBAL}</tt>.
	 * 
	 * @param contract
	 *            the context consumer contract.
	 * @param consumer
	 *            the context consumer.
	 * @param ack
	 *            the boolean stating whether an acknowledgement is expected.
	 * @return the result of the command.
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem.
	 */
	CommandResult setContextConsumerContract(ContextConsumerContract contract,
			ContextConsumer consumer, ACK ack) throws MuDEBSException;

	/**
	 * sets a context consumer contract. This call is non-blocking and no
	 * acknowledgement is waited for.
	 * 
	 * @param contract
	 *            the context consumer contract.
	 * @param consumer
	 *            the context consumer.
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem.
	 */
	void setContextConsumerContract(ContextConsumerContract contract,
			ContextConsumer consumer) throws MuDEBSException;

	/**
	 * resets all the context consumer contracts. This call is blocking when
	 * setting the argument <tt>ack</tt> to <tt>ACK.{LOCAL|GLOBAL}</tt>.
	 * 
	 * @param ack
	 *            the boolean stating whether an acknowledgement is expected.
	 * @return the result of the command.
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem.
	 */
	CommandResult unsetAllContextConsumerContracts(ACK ack)
			throws MuDEBSException;

	/**
	 * resets all the context consumer contracts. This call is non-blocking and
	 * no acknowledgement is waited for.
	 * 
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem.
	 */
	void unsetAllContextConsumerContracts() throws MuDEBSException;

	/**
	 * processes the context data received.
	 * 
	 * @param contextData
	 *            the context data received
	 */
	void consume(PublicationMsgContent contextData);

	/**
	 * sends a synchronous anonymous request with multiple replies. This is a
	 * request, that is it is the pull mode for obtaining context data. It is
	 * synchronous because the call does not return without getting context data
	 * from all the potential producers. In addition, it is anonymous because
	 * the addressees are unknown. The collect of context data may be
	 * constrained by privacy guarantees that the requester may provide. In
	 * addition, the request leads to a result containing multiple replies from
	 * multiple producers. Finally, the request may be local or global, that is
	 * the collect of publications is local to the access broker of the
	 * requester or spans all the brokers.
	 * 
	 * Contrary to a subscription, the contract is not stored and serves only
	 * once ---i.e., for this call.
	 * 
	 * @param contract
	 *            the identifier of the context consumer contract.
	 * @param abacInfo
	 *            the ABAC information to have access to context data with
	 *            privacy requirements.
	 * @return the replies of the request.
	 * @throws MuDEBSException
	 *             the exception thrown in case of problem.
	 */
	List<Reply> request(ContextConsumerContract contract,
			final ABACInformation abacInfo) throws MuDEBSException;

	/**
	 * shuts down the context capsule.
	 */
	void terminate();

	/**
	 * gets the raw message corresponding to the lastly received context data.
	 * 
	 * NB: this method is used when the client needs header information about
	 * the body of the last message received.
	 * 
	 * @return the content of the publication.
	 */
	PublicationMsgContent getRawMessage();
}

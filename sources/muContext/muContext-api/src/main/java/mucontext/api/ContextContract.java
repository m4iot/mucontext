/**
This file is part of the muContext middleware.

Copyright (C) 2014-2017 Télécom SudParis

The INCOME platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The INCOME platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the INCOME platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */

package mucontext.api;

import mudebs.common.algorithms.OperationalMode;
import mudebs.common.algorithms.routing.MultiScopingSpecification;

/**
 * This interface defines a generic context contract.
 * 
 * @author Denis Conan
 * @author Léon Lim
 */
public interface ContextContract {
	/**
	 * gets the identifier of the context contract.
	 * 
	 * @return the identifier of the contract.
	 */
	String identifier();

	/**
	 * gets the boolean stating whether the subscription is local. Please refer
	 * to the sub-interfaces for the meaning of a local or a global contract.
	 * 
	 * @return the operation mode of the contract: global or local.
	 */
	OperationalMode operationalMode();

	/**
	 * gets the multi-scoping specification, that is the set of scopes. This set
	 * can be <tt>null</tt>.
	 * 
	 * @return the multiscoping information of the contract.
	 */
	MultiScopingSpecification mulscopingSpecification();

	/**
	 * gets the script of the muDEBS routing filter of the context contract.
	 * 
	 * @return the JavaScript of the filter of the contract.
	 */
	String routingFilterScript();
}

package mucontext.api;
/**
This file is part of the muDEBS middleware.

Copyright (C) 2014-2015 Télécom SudParis

The muDEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The muDEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the INCOME platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */


import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * This class contains the configuration of the logging facilities provided in
 * muContext.
 * 
 * To recapitulate, logging levels are: TRACE, DEBUG, INFO, WARN, ERROR, FATAL.
 * 
 * @author Denis Conan
 * 
 */
public final class Log {
	/**
	 * states whether logging is enabled or not.
	 */
	public static final boolean ON = true;
	/**
	 * logger object for the communication part.
	 */
	public static final Logger COMM = Logger.getLogger("muContext-communication");
	/**
	 * logger object for the configuration part.
	 */
	public static final Logger CONFIG = Logger.getLogger("muContext-configuration");
	/**
	 * logger object for the scripting part.
	 */
	public static final Logger SCRIPTING = Logger.getLogger("    muContext-scripting");

	/**
	 * static configuration, which can be changed by command line options.
	 */
	static {
		COMM.setLevel(Level.WARN);
		CONFIG.setLevel(Level.WARN);
		SCRIPTING.setLevel(Level.WARN);
	}

	/**
	 * configures a logger to a level.
	 * 
	 * @param loggerName
	 *            the name of the logger.
	 * @param levelName
	 *            the name of the level.
	 */
	public static void configureALogger(final String loggerName,
			final String levelName) {
		if (loggerName == null || levelName == null) {
			return;
		}
		if (loggerName.equalsIgnoreCase("COMM")) {
			if (levelName.equalsIgnoreCase("TRACE")) {
				COMM.setLevel(Level.TRACE);
				mudebs.common.Log.COMM.setLevel(Level.TRACE);
			} else if (levelName.equalsIgnoreCase("DEBUG")) {
				COMM.setLevel(Level.DEBUG);
				mudebs.common.Log.COMM.setLevel(Level.DEBUG);
			} else if (levelName.equalsIgnoreCase("INFO")) {
				COMM.setLevel(Level.INFO);
				mudebs.common.Log.COMM.setLevel(Level.INFO);
			} else if (levelName.equalsIgnoreCase("WARN")) {
				COMM.setLevel(Level.WARN);
				mudebs.common.Log.COMM.setLevel(Level.WARN);
			} else if (levelName.equalsIgnoreCase("ERROR")) {
				COMM.setLevel(Level.ERROR);
				mudebs.common.Log.COMM.setLevel(Level.ERROR);
			} else if (levelName.equalsIgnoreCase("FATAL")) {
				COMM.setLevel(Level.FATAL);
				mudebs.common.Log.COMM.setLevel(Level.FATAL);
			}
		} else if (loggerName.equalsIgnoreCase("CONFIG")) {
			if (levelName.equalsIgnoreCase("TRACE")) {
				CONFIG.setLevel(Level.TRACE);
				mudebs.common.Log.CONFIG.setLevel(Level.TRACE);
			} else if (levelName.equalsIgnoreCase("DEBUG")) {
				CONFIG.setLevel(Level.DEBUG);
				mudebs.common.Log.CONFIG.setLevel(Level.DEBUG);
			} else if (levelName.equalsIgnoreCase("INFO")) {
				CONFIG.setLevel(Level.INFO);
				mudebs.common.Log.CONFIG.setLevel(Level.INFO);
			} else if (levelName.equalsIgnoreCase("WARN")) {
				CONFIG.setLevel(Level.WARN);
				mudebs.common.Log.CONFIG.setLevel(Level.WARN);
			} else if (levelName.equalsIgnoreCase("ERROR")) {
				CONFIG.setLevel(Level.ERROR);
				mudebs.common.Log.CONFIG.setLevel(Level.ERROR);
			} else if (levelName.equalsIgnoreCase("FATAL")) {
				CONFIG.setLevel(Level.FATAL);
				mudebs.common.Log.CONFIG.setLevel(Level.FATAL);
			}
		} else if (loggerName.equalsIgnoreCase("SCRIPTING")) {
			if (levelName.equalsIgnoreCase("TRACE")) {
				SCRIPTING.setLevel(Level.TRACE);
				mudebs.common.Log.SCRIPTING.setLevel(Level.TRACE);
			} else if (levelName.equalsIgnoreCase("DEBUG")) {
				SCRIPTING.setLevel(Level.DEBUG);
				mudebs.common.Log.SCRIPTING.setLevel(Level.DEBUG);
			} else if (levelName.equalsIgnoreCase("INFO")) {
				SCRIPTING.setLevel(Level.INFO);
				mudebs.common.Log.SCRIPTING.setLevel(Level.INFO);
			} else if (levelName.equalsIgnoreCase("WARN")) {
				SCRIPTING.setLevel(Level.WARN);
				mudebs.common.Log.SCRIPTING.setLevel(Level.WARN);
			} else if (levelName.equalsIgnoreCase("ERROR")) {
				SCRIPTING.setLevel(Level.ERROR);
				mudebs.common.Log.SCRIPTING.setLevel(Level.ERROR);
			} else if (levelName.equalsIgnoreCase("FATAL")) {
				SCRIPTING.setLevel(Level.FATAL);
				mudebs.common.Log.SCRIPTING.setLevel(Level.FATAL);
			}
		}
	}

	public static String printStackTrace(StackTraceElement[] stack) {
		String result = "";
		for (int i = 0; i < stack.length; i++) {
			result += stack[i] + "\n";
		}
		return result;
	}
}

/**
This file is part of the muContext middleware.

Copyright (C) 2014-2017 Télécom SudParis

The INCOME platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The INCOME platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the INCOME platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */

package mucontext.api;

import mudebs.common.filters.XACMLPolicy;

/**
 * This interface defines a generic context producer contract.
 * 
 * The contract may be local or global. In other words, since a producer
 * contract leads to a muDEBS advertisement, the advertisement may be local or
 * global. More precisely, a local advertisement is deployed only in the access
 * broker of the producer. This implies that a publications containing context
 * data that matches the advertisement must be transported to the access broker
 * of the consumer after the test of the matching. A global advertisement is
 * deployed in all the brokers and a distributed routing algorithm implement the
 * transportation of the context data from this producer to the all the
 * consumers by using the deployed advertisement.
 * 
 * @author Denis Conan
 */
public interface ContextProducerContract extends ContextContract {
	/**
	 * get the XACML policy.
	 * 
	 * @return the XACML policy.
	 */
	XACMLPolicy policy();
}

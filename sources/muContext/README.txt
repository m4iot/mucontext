muContext platform

Copyright (C) 2013-2017
Contact: Denis.Conan[at]telecom-sudparis.eu

This product includes software developed at Institut Mines Télécom /
Télécom SudParis, except for the maven modules named xacml-unknown
(see the copyrights in the Java class files).

License: See file LICENSE.txt

================================================================================

Content of this file :
- Content of this directory
- Software prerequisites
- Shell variables to set
- Compilation and installation
- Execution
- Miscellaneous

NB: shell commands of this file are prepared to be executed
    in this working directory.

================================================================================

Content of this directory:
--------------------------
	- file "LICENSE.txt": points to LGPL
	- file "COPYING.LESSER": GNU LGPL Version 3
	- file "pom.xml": contains the Project Object Model of the root
	  of the Maven modules
	- file "README.txt": is this file
	- file "RELEASE_NOTES.txt": contains information for releases

Software prerequisites:
-----------------------
("opt" is for "optional" for Maven installation):
	1. Java Version 6.0
	   (http://www.oracle.com/technetwork/java/javase/overview/index.html)
	2. Maven Version 3.0.4
	   (http://maven.apache.org/)
opt	3. Eclipse Version 4.3 Kepler
	   (http://eclipse.org/)
opt	4. Apache Felix
	   (http://felix.apache.org/)
opt	5. RabbitMQ Version rabbitmq_server-3.1.5
	   (http://www.rabbitmq.com/install-generic-unix.html)
	6. Unix shell
	   - The scripts have been tested on GNU/Linux operating systems,
	     more especially on Debian distribution, Version Jessie
	7. muDEBS framework
	   - current release used is 0.17.1
	   - in case of difficulties for uploading the maven artefacts
	     through the HTTPS connection, add the following parameters
	     to the maven command:
       "-Djsse.enableSNIExtension=false"
       "-Dmaven.wagon.http.ssl.insecure=true"
       "-Dmaven.wagon.http.ssl.allowall=true"


Shell variables to set:
-----------------------
	1. JAVA_HOME to your Java SDK 6.0
$
export JAVA_HOME=<the root directory of your Java installation>
echo $JAVA_HOME
$
	2. CLASSPATH to your felix.jar of Felix
export CLASSPATH=$CLASSPATH:<the root directory of your Felix installation>/bin/felix.jar
	3. RMQ_HOME to your rabbitmq_server-3.1.5
$
export RMQ_HOME=<the root directory of your RabbitMQ installation>
PATH=$PATH:${RMQ_HOME}/rabbitmq_server-3.1.5/sbin
export RABBITMQ_MNESIA_BASE=${RMQ_HOME}/rabbitmq_server-3.1.5/mnesia
export RABBITMQ_LOG_BASE=${RMQ_HOME}/rabbitmq_server-3.1.5/log
$

Compilation and installation:
-----------------------------
	To compile and install the modules, use the script install.sh or execute the 
	following command. Use the options 
	-Dmaven.test.skip=true and -Dmaven.javadoc.skip=true
	if you do not want to execute the JUnit tests or to generate the
	JavaDoc files, so that the compilation is much more rapid.

    The maven compilation and installation needs to be made in two steps,
    first the clean goal and then the install goal.
    Warning: "mvn clean install" does not work because the installation of
             the muContext-metamodel module generates files that are actually
             immediately removed if the clean goal is not executed separately.

$
mvn clean && mvn -Djsse.enableSNIExtension=false install
$



Miscellaneous:
--------------
Installation of the muContext model driven engineering plugins

The muContext project provides some plugins for the Eclipse IDE. 
For the moment, these plugins are only supposed to be compatible with the 
Eclipse Kepler 4.3 release.

  The following update site can be used to install the muContext plugins:

  https://fusionforge.int-evry.fr/www/mucontext/updatesite/

  WARNING: In order to be able to access the update site, Eclipse needs to be 
  launched with the following command:

$ 
eclipse -vmargs -Djsse.enableSNIExtension=false
$       

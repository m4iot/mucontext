/**
This file is part of the muContext middleware.

Copyright (C) 2013-2014 Télécom SudParis

The INCOME platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The INCOME platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the INCOME platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Xuan Li, Léon Lim
 */

package mucontext.contextcapsule;

import java.io.StringReader;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.TransformerException;

import mucontext.api.ContextCapsule;
import mucontext.api.ContextConsumer;
import mucontext.api.ContextConsumerContract;
import mucontext.api.ContextProducerContract;
import mucontext.api.Log;
import mudebs.client.ClientDelegate;
import mudebs.common.Constants;
import mudebs.common.MuDEBSException;
import mudebs.common.algorithms.ACK;
import mudebs.common.algorithms.CommandResult;
import mudebs.common.algorithms.CommandStatus;
import mudebs.common.algorithms.Reply;
import mudebs.common.algorithms.routing.ABACInformation;
import mudebs.common.algorithms.routing.PublicationMsgContent;
import mudebs.common.filters.JavaScriptRoutingFilter;
import mudebs.common.filters.RoutingFilter;

import org.apache.log4j.Level;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

/**
 * This class defines a generic context capsule, which sets and resets context
 * producer and context consumer contracts, and pushes and pulls context data.
 * 
 * @author Denis Conan
 * @author Xuan Li
 * @author Léon Lim
 * 
 */
public class BasicContextCapsule implements ContextCapsule {

	/**
	 * URI of the client, which is provided as a command line argument.
	 */
	private static String uri = null;

	/**
	 * URI of the broker to which the client is connected. The URI is provided
	 * as a command line argument.
	 */
	private static String broker = null;

	/**
	 * the client delegate.
	 */
	private static ClientDelegate delegate;

	/**
	 * script engine for JavaScript methods of filters.
	 */
	public ScriptEngineManager scriptEngineManager;

	/**
	 * the set of consumer handlers.
	 */
	private static HashMap<ContextConsumer, RoutingFilter> consumerContracts;

	/**
	 * Constructor of the context capsule.
	 * 
	 * @param commandLineArguments
	 *            The arguments of the context capsule, which are seen as
	 *            command line arguments, that is to say as an array of strings.
	 * @throws MuDEBSException
	 *             The muDEBS exception thrown when the context capsule cannot
	 *             be instantiated due a muDEBS problem.
	 */
	public BasicContextCapsule(final String[] commandLineArguments)
			throws MuDEBSException {
		configure(commandLineArguments);
		if (uri == null) {
			if (Log.ON && Log.CONFIG.isEnabledFor(Level.FATAL)) {
				Log.CONFIG.fatal("Cannot create"
						+ " a client delegate with a null identity");
			}
			throw new MuDEBSException("Cannot create"
					+ " a client delegate with a null identity");
		}
		if (broker == null) {
			if (Log.ON && Log.CONFIG.isEnabledFor(Level.FATAL)) {
				Log.CONFIG.fatal(uri + ", cannot create"
						+ " a client delegate with a null broker URI");
			}
			throw new MuDEBSException(uri + ", cannot create"
					+ " a client delegate with a null broker URI");
		}
		delegate = new ClientDelegate(uri, broker, this);
		delegate.startMessageDispatcher();
		scriptEngineManager = new ScriptEngineManager();
		consumerContracts = new HashMap<ContextConsumer, RoutingFilter>();
		delegate.waitConnectionToAccessBroker();
	}

	/**
	 * configures the client.
	 */
	private static void configure(final String[] argv) {
		Vector<String> loggers = new Vector<String>();
		for (int i = 0; i < argv.length; i = i + 2) {
			if (argv[i].equalsIgnoreCase(Constants.OPTION_URI)) {
				uri = argv[i + 1];
			} else if (argv[i].equalsIgnoreCase(Constants.OPTION_BROKER)) {
				broker = argv[i + 1];
			} else if (argv[i].equalsIgnoreCase(Constants.OPTION_LOGGER)) {
				loggers.add(argv[i + 1]);
			}
		}
		if ((uri == null) || (broker == null)) {
			if (Log.ON && Log.CONFIG.isEnabledFor(Level.FATAL)) {
				Log.CONFIG
						.fatal("URI of the client or the broker not provided");
				Log.CONFIG.fatal("usage: java ClientMain "
						+ Constants.OPTION_URI + " <URI of this client> "
						+ Constants.OPTION_BROKER + " <URI of the broker>"
						+ Constants.OPTION_LOGGER + " <logger.level>...");
			}
			System.exit(-1);
		}
		for (String logger : loggers) {
			String[] conf = logger.split("\\.");
			if (conf.length == 2) {
				Log.configureALogger(conf[0], conf[1]);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mucontext.api.ContextCapsule#setContextProducerContract(mucontext.api
	 * .ContextProducerContract)
	 */
	@Override
	public final void setContextProducerContract(
			final ContextProducerContract contract) throws MuDEBSException, TransformerException {
		setContextProducerContract(contract, ACK.NO);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mucontext.api.ContextCapsule#setContextProducerContract(mucontext.api
	 * .ContextProducerContract, mudebs.common.algorithms.ACK)
	 */
	@Override
	public final CommandResult setContextProducerContract(
			final ContextProducerContract contract, final ACK ack)
			throws MuDEBSException, TransformerException {
		if (contract == null) {
			throw new MuDEBSException(uri + ", missing contract (" + contract
					+ ")");
		}
		if (contract.identifier() == null || contract.identifier().equals("")
				|| contract.routingFilterScript() == null
				|| contract.routingFilterScript().equals("")) {
			throw new MuDEBSException(uri
					+ ", some of the argument is missing (identifier="
					+ contract.identifier() + ", script="
					+ contract.routingFilterScript() + ")");
		}
		if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
			Log.COMM.debug(uri + ", setting context producer contract "
					+ contract.identifier());
		}
		if (Log.ON && Log.COMM.isEnabledFor(Level.TRACE)) {
			Log.COMM.trace(uri + ", setting context producer contract "
					+ contract);
		}
		return delegate.advertise(contract.identifier(), contract
				.operationalMode(), ack, contract.routingFilterScript(),
				contract.mulscopingSpecification(),
				(contract.policy() == null ? null : contract.policy()
						.getThePolicy()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mucontext.api.ContextCapsule#unsetContextProducerContract(java.lang.String
	 * )
	 */
	@Override
	public final void unsetContextProducerContract(final String identifier)
			throws MuDEBSException {
		unsetContextProducerContract(identifier, ACK.NO);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mucontext.api.ContextCapsule#unsetContextProducerContract(java.lang.String
	 * , mudebs.common.algorithms.ACK)
	 */
	@Override
	public final CommandResult unsetContextProducerContract(
			final String identifier, final ACK ack) throws MuDEBSException {
		if (identifier == null || identifier.equals("")) {
			throw new MuDEBSException(uri + ", identifier argument is missing");
		}
		if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
			Log.COMM.debug(uri + ", unsetting context producer contract "
					+ identifier);
		}
		return delegate.unadvertise(identifier, ack);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mucontext.api.ContextCapsule#unsetAllContextProducerContracts()
	 */
	@Override
	public final void unsetAllContextProducerContracts() throws MuDEBSException {
		unsetAllContextProducerContracts(ACK.NO);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mucontext.api.ContextCapsule#unsetAllContextProducerContracts(mudebs.
	 * common.algorithms.ACK)
	 */
	@Override
	public final CommandResult unsetAllContextProducerContracts(final ACK ack)
			throws MuDEBSException {
		if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
			Log.COMM.debug(uri + ", all the context producer contracts");
		}
		return delegate.unadvertiseAll(ack);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mucontext.api.ContextCapsule#push(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public final void push(final String identifier, final String contextData)
			throws MuDEBSException {
		push(identifier, contextData, ACK.NO);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mucontext.api.ContextCapsule#push(java.lang.String,
	 * java.lang.String, mudebs.common.algorithms.ACK)
	 */
	@Override
	public final CommandResult push(final String identifier,
			final String contextData, final ACK ack) throws MuDEBSException {
		if (identifier == null || identifier.equals("") || contextData == null
				|| contextData.equals("")) {
			throw new MuDEBSException(uri
					+ ", some of the argument is missing (identifier="
					+ identifier + ", contextData=" + contextData + ")");
		}
		if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
			Log.COMM.debug(uri + ", pushing" + " context data for contract: "
					+ identifier);
		}
		if (Log.ON && Log.COMM.isEnabledFor(Level.TRACE)) {
			Log.COMM.trace(uri + ", pushing" + " context data: " + contextData);
		}
		return delegate.publish(identifier, ack, contextData);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mucontext.api.ContextCapsule#setContextConsumerContract(mucontext.api
	 * .ContextConsumerContract, mucontext.api.ContextConsumer)
	 */
	@Override
	public final void setContextConsumerContract(
			final ContextConsumerContract contract,
			final ContextConsumer consumer) throws MuDEBSException {
		setContextConsumerContract(contract, consumer, ACK.NO);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mucontext.api.ContextCapsule#setContextConsumerContract(mucontext.api
	 * .ContextConsumerContract, mucontext.api.ContextConsumer,
	 * mudebs.common.algorithms.ACK)
	 */
	@Override
	public final CommandResult setContextConsumerContract(
			final ContextConsumerContract contract,
			final ContextConsumer consumer, final ACK ack)
			throws MuDEBSException {
		if (contract == null) {
			throw new MuDEBSException(uri + ", missing contract (" + contract
					+ ")");
		}
		if (contract.identifier() == null || contract.identifier().equals("")
				|| contract.routingFilterScript() == null
				|| contract.routingFilterScript().equals("")) {
			throw new MuDEBSException(uri
					+ ", some of the argument is missing (identifier="
					+ contract.identifier() + ", filter="
					+ contract.routingFilterScript() + ")");
		}
		if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
			Log.COMM.debug(uri + ", setting context consumer contract "
					+ contract.identifier());
		}
		if (Log.ON && Log.COMM.isEnabledFor(Level.TRACE)) {
			Log.COMM.trace(uri + ", setting context consumer contract "
					+ contract);
		}
		ScriptEngine engine = scriptEngineManager.getEngineByName("JavaScript");
		try {
			engine.eval(contract.routingFilterScript());
		} catch (ScriptException e) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.ERROR)) {
				Log.COMM.error(uri + ", "
						+ Log.printStackTrace(e.getStackTrace()));
			}
			return new CommandResult(CommandStatus.ERROR,
					"problem in compiling"
							+ " and installing the JavaScript filter");
		}
		Invocable inv = (Invocable) engine;
		JavaScriptRoutingFilter mf = inv
				.getInterface(JavaScriptRoutingFilter.class);
		if (Log.ON && Log.COMM.isEnabledFor(Level.TRACE)) {
			Log.COMM.trace(uri
					+ ", inv.getInterface(MonolithicFilter.class) returned "
					+ mf);
		}
		consumerContracts.put(consumer, mf);
		return delegate.subscribe(contract.identifier(),
				contract.operationalMode(), ack,
				contract.routingFilterScript(),
				contract.mulscopingSpecification(), contract.abacInformation());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mucontext.api.ContextCapsule#unsetAllContextConsumerContracts()
	 */
	@Override
	public final void unsetAllContextConsumerContracts() throws MuDEBSException {
		unsetAllContextConsumerContracts(ACK.NO);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mucontext.api.ContextCapsule#unsetAllContextConsumerContracts(mudebs.
	 * common.algorithms.ACK)
	 */
	@Override
	public final CommandResult unsetAllContextConsumerContracts(final ACK ack)
			throws MuDEBSException {
		if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
			Log.COMM.debug(uri + ", resetting"
					+ " all the context consumer contracts");
		}
		return delegate.unsubscribeAll(ack);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mucontext.api.ContextCapsule#consume(mudebs.common.algorithms.routing
	 * .PublicationMsgContent)
	 */
	@Override
	public final void consume(final PublicationMsgContent contextData) {
		boolean consumerFound = false;
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		Document doc = null;
		try {
			builder = factory.newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(contextData.getContent()));
			doc = builder.parse(is);
		} catch (Exception e) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.ERROR)) {
				Log.COMM.error("error when handling publication:"
						+ e.getMessage());
			}
		}
		for (ContextConsumer consumer : consumerContracts.keySet()) {
			if (consumerContracts.get(consumer).evaluate(doc)) {
				if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
					Log.COMM.debug(uri + ", consuming"
							+ " context data for contract: "
							+ contextData.getIdAdv());
				}
				if (Log.ON && Log.COMM.isEnabledFor(Level.TRACE)) {
					Log.COMM.trace(uri + ", consuming" + " context data: "
							+ contextData.getContent());
				}
				consumerFound = true;
				consumer.consume(contextData.getContent());
			}
		}
		if (!consumerFound) {
			if (Log.ON && Log.COMM.isEnabledFor(Level.WARN)) {
				Log.COMM.warn(uri + ", unknown consumer contract: "
						+ contextData.getIdAdv());
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mucontext.api.ContextCapsule#terminate()
	 */
	@Override
	public final void terminate() {
		if (Log.ON && Log.CONFIG.isEnabledFor(Level.DEBUG)) {
			Log.CONFIG.debug(uri + ", shutting down the context capsule");
		}
		delegate.terminate();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mucontext.api.ContextCapsule#getRawMessage()
	 */
	@Override
	public PublicationMsgContent getRawMessage() {
		return delegate.getLastPublication();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mucontext.api.ContextCapsule#request(java.lang.String)
	 */
	@Override
	public List<Reply> request(ContextConsumerContract contract,
			final ABACInformation abacInfo) throws MuDEBSException {
		if (contract == null) {
			throw new MuDEBSException(uri + ", missing contract (" + contract
					+ ")");
		}
		if (contract.identifier() == null || contract.identifier().equals("")
				|| contract.routingFilterScript() == null
				|| contract.routingFilterScript().equals("")) {
			throw new MuDEBSException(uri
					+ ", some of the argument is missing (identifier="
					+ contract.identifier() + ", filter="
					+ contract.routingFilterScript() + ")");
		}
		if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
			Log.COMM.debug(uri + ", starting a request with "
					+ "context consumer contract " + contract.identifier());
		}
		if (Log.ON && Log.COMM.isEnabledFor(Level.TRACE)) {
			Log.COMM.trace(uri + ", context consumer contract = " + contract);
		}
		return delegate.request(contract.operationalMode(),
				contract.routingFilterScript(),
				contract.mulscopingSpecification(), abacInfo);
	}
}
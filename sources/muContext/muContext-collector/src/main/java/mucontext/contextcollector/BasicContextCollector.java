/**
This file is part of the muContext middleware.

Copyright (C) 2014-2017 Télécom SudParis

The INCOME platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The INCOME platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the INCOME platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Xuan Li, Léon Lim
 */

package mucontext.contextcollector;

import java.util.Vector;

import javax.xml.transform.TransformerException;

import mucontext.api.ContextCollector;
import mucontext.api.ContextProducerContract;
import mucontext.api.Log;
import mudebs.client.ClientDelegate;
import mudebs.common.Constants;
import mudebs.common.MuDEBSException;
import mudebs.common.algorithms.ACK;
import mudebs.common.algorithms.CommandResult;

import org.apache.log4j.Level;

/**
 * This class defines a generic context collector, which sets and resets context
 * producer contracts, and pushes context data.
 * 
 * @author Denis Conan
 * @author Xuan Li
 * @author Léon Lim
 * 
 */
public class BasicContextCollector implements ContextCollector {

	/**
	 * URI of the client, which is provided as a command line argument.
	 */
	private static String uri = null;

	/**
	 * URI of the broker to which the client is connected. The URI is provided
	 * as a command line argument.
	 */
	private static String broker = null;

	/**
	 * the client delegate.
	 */
	private static ClientDelegate delegate;

	/**
	 * Constructor of the context collector.
	 * 
	 * @param commandLineArguments
	 *            The arguments of the context collector, which are seen as
	 *            command line arguments, that is to say as an array of strings.
	 * @throws MuDEBSException
	 *             the exception thrown by muDEBS.
	 */
	public BasicContextCollector(final String[] commandLineArguments)
			throws MuDEBSException {
		configure(commandLineArguments);
		if (uri == null) {
			if (Log.ON && Log.CONFIG.isEnabledFor(Level.FATAL)) {
				Log.CONFIG.fatal("Cannot create"
						+ " a client delegate with a null identity");
			}
			throw new MuDEBSException("Cannot create"
					+ " a client delegate with a null identity");
		}
		if (broker == null) {
			if (Log.ON && Log.CONFIG.isEnabledFor(Level.FATAL)) {
				Log.CONFIG.fatal(uri + ", cannot create"
						+ " a client delegate with a null broker URI");
			}
			throw new MuDEBSException(uri + ", cannot create"
					+ " a client delegate with a null broker URI");
		}
		delegate = new ClientDelegate(uri, broker, null);
		delegate.startMessageDispatcher();
		delegate.waitConnectionToAccessBroker();
	}

	/**
	 * configures the client.
	 */
	private final void configure(final String[] argv) {
		Vector<String> loggers = new Vector<String>();
		for (int i = 0; i < argv.length; i = i + 2) {
			if (argv[i].equalsIgnoreCase(Constants.OPTION_URI)) {
				uri = argv[i + 1];
			} else if (argv[i].equalsIgnoreCase(Constants.OPTION_BROKER)) {
				broker = argv[i + 1];
			} else if (argv[i].equalsIgnoreCase(Constants.OPTION_LOGGER)) {
				loggers.add(argv[i + 1]);
			}
		}
		if ((uri == null) || (broker == null)) {
			if (Log.ON && Log.CONFIG.isEnabledFor(Level.FATAL)) {
				Log.CONFIG
						.fatal("URI of the client or the broker not provided");
				Log.CONFIG.fatal("usage: java ClientMain "
						+ Constants.OPTION_URI + " <URI of this client> "
						+ Constants.OPTION_BROKER + " <URI of the broker>"
						+ Constants.OPTION_LOGGER + " <logger.level>...");
			}
			System.exit(-1);
		}
		for (String logger : loggers) {
			String[] conf = logger.split("\\.");
			if (conf.length == 2) {
				Log.configureALogger(conf[0], conf[1]);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mucontext.api.ContextCollector#setContextProducerContract(mucontext.api
	 * .ContextProducerContract)
	 */
	public final void setContextProducerContract(
			final ContextProducerContract contract)
			throws MuDEBSException, TransformerException {
		setContextProducerContract(contract, ACK.NO);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mucontext.api.ContextCollector#setContextProducerContract(mucontext.api
	 * .ContextProducerContract, mudebs.common.algorithms.ACK)
	 */
	public final CommandResult setContextProducerContract(
			final ContextProducerContract contract, final ACK ack)
			throws MuDEBSException, TransformerException {
		if (contract == null) {
			throw new MuDEBSException(
					uri + ", missing contract (" + contract + ")");
		}
		if (contract.identifier() == null || contract.identifier().equals("")
				|| contract.routingFilterScript() == null
				|| contract.routingFilterScript().equals("")) {
			throw new MuDEBSException(
					uri + ", some of the argument is missing (identifier="
							+ contract.identifier() + ", filter="
							+ contract.routingFilterScript() + ")");
		}
		if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
			Log.COMM.debug(uri + ", setting context producer contract "
					+ contract.identifier());
		}
		if (Log.ON && Log.COMM.isEnabledFor(Level.TRACE)) {
			Log.COMM.trace(
					uri + ", setting context producer contract " + contract);
		}
		return delegate.advertise(contract.identifier(),
				contract.operationalMode(), ack, contract.routingFilterScript(),
				contract.mulscopingSpecification(), (contract.policy() == null
						? null : contract.policy().getThePolicy()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mucontext.api.ContextCollector#unsetContextProducerContract(java.lang
	 * .String)
	 */
	public final void unsetContextProducerContract(final String identifier)
			throws MuDEBSException {
		unsetContextProducerContract(identifier, ACK.NO);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mucontext.api.ContextCollector#unsetContextProducerContract(java.lang
	 * .String, mudebs.common.algorithms.ACK)
	 */
	public final CommandResult unsetContextProducerContract(
			final String identifier, final ACK ack) throws MuDEBSException {
		if (identifier == null || identifier.equals("")) {
			throw new MuDEBSException(uri + ", identifier argument is missing");
		}
		if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
			Log.COMM.debug(uri + ", unsetting context producer contract "
					+ identifier);
		}
		return delegate.unadvertise(identifier, ack);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mucontext.api.ContextCollector#unsetAllContextProducerContracts()
	 */
	public final void unsetAllContextProducerContracts()
			throws MuDEBSException {
		unsetAllContextProducerContracts(ACK.NO);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mucontext.api.ContextCollector#unsetAllContextProducerContracts(mudebs
	 * .common.algorithms.ACK)
	 */
	public final CommandResult unsetAllContextProducerContracts(final ACK ack)
			throws MuDEBSException {
		if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
			Log.COMM.debug(uri + ", all the context producer contracts");
		}
		return delegate.unadvertiseAll(ack);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mucontext.api.ContextCollector#push(java.lang.String,
	 * java.lang.String)
	 */
	public final void push(final String identifier, final String contextData)
			throws MuDEBSException {
		push(identifier, contextData, ACK.NO);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mucontext.api.ContextCollector#push(java.lang.String,
	 * java.lang.String, mudebs.common.algorithms.ACK)
	 */
	public final CommandResult push(final String identifier,
			final String contextData, final ACK ack) throws MuDEBSException {
		if (identifier == null || identifier.equals("") || contextData == null
				|| contextData.equals("")) {
			throw new MuDEBSException(uri
					+ ", some of the argument is missing (identifier="
					+ identifier + ", contextData=" + contextData + ")");
		}
		if (Log.ON && Log.COMM.isEnabledFor(Level.DEBUG)) {
			Log.COMM.debug(uri + ", pushing" + " context data for contract: "
					+ identifier);
		}
		if (Log.ON && Log.COMM.isEnabledFor(Level.TRACE)) {
			Log.COMM.trace(uri + ", pushing" + " context data: " + contextData);
		}
		return delegate.publish(identifier, ack, contextData);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mucontext.api.ContextCollector#shutdown()
	 */
	public final void shutdown() {
		if (Log.ON && Log.CONFIG.isEnabledFor(Level.DEBUG)) {
			Log.CONFIG.debug(uri + ", shutting down the context application");
		}
		delegate.terminate();
	}
}

/**
This file is part of the muContext middleware.

Copyright (C) 2014-2015 Télécom SudParis

The INCOME platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The INCOME platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the INCOME platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Pierrick MARIE
 */

package mucontext.datamodel.context;

import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.Vector;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import mucontext.datamodel.multiscalability.Dimension;
import mucontext.datamodel.multiscalability.EventDistributionRange;
import mucontext.datamodel.multiscalability.Measure;
import mucontext.datamodel.multiscalability.MultiscalabilityDataModelFacade;
import mucontext.datamodel.multiscalability.MultiscalabilityReport;
import mucontext.datamodel.multiscalability.ProjectionValue;
import mucontext.datamodel.multiscalability.Scale;
import mucontext.datamodel.multiscalability.ScaleSet;
import mucontext.datamodel.multiscalability.Viewpoint;
import mudebs.common.filters.JavaScriptRoutingFilter;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class TestAll {

	private static ScriptEngine addNewFilter(final ScriptEngine engine)
			throws ScriptException {
		engine.eval("function evaluate(doc) {"
				+ "load(\"nashorn:mozilla_compat.js\");"
				+ "importPackage(javax.xml.xpath);"
				+ "var xpath = XPathFactory.newInstance().newXPath();"
				+ "var node = xpath.evaluate('//observable/name', doc, XPathConstants.NODE);"
				+ "return node.getTextContent().equals('lifetime');}");
		return engine;
	}

	/**
	 * script engine for JavaScript methods of filters.
	 */
	public ScriptEngineManager scriptEngineManager;

	@Before
	public void setup() throws JAXBException {
		scriptEngineManager = new ScriptEngineManager();
	}

	@Test
	public void testWithFacade() throws URISyntaxException, JAXBException {
		ContextDataModelFacade facade = new ContextDataModelFacade("facade");
		EntityRelationType typeBelongsTo = facade
				.createEntityRelationType("typeBelongsto");
		ContextEntity sophie = facade.createContextEntity("sophie",
				new URI("mucontext://localhost:3000/sophie"));
		ContextEntity battery = facade.createContextEntity("battery",
				new URI("mucontext://localhost:3000/battery"));
		Vector<ContextEntity> entities = new Vector<ContextEntity>();
		entities.add(battery);
		entities.add(sophie);
		facade.createBinaryEntityRelation("belongsTo", typeBelongsTo, entities);
		ContextObservable lifetime = facade.createContextObservable("lifetime",
				new URI("mucontext://localhost:3000/battery/lifetime"),
				battery);
		ContextObservation<?> observation1 = facade.createContextObservation(
				"observation1", new Double(0.75), lifetime);
		ContextReport report = facade.createContextReport("report");
		facade.addContextObservation(report, observation1);
		MultiscalabilityDataModelFacade mfacade = new MultiscalabilityDataModelFacade(
				"facade");
		Viewpoint viewpoint = mfacade.CreateViewpoint("DEBS");
		Dimension dimension = mfacade
				.createDimension("event distribution range", viewpoint);
		Measure<?> measure = mfacade.createMeasure(
				"event distribution range measure",
				EventDistributionRange.UNIVERSE);
		ScaleSet scaleset = mfacade.createScaleSet(
				"event distribution range"
						+ " in event distribution range measure scaleset",
				dimension, measure);
		Scale scale = mfacade.createScale("universe", scaleset);
		ProjectionValue projectionvalue = mfacade.createProjectionValue("pv",
				scale);
		MultiscalabilityReport mr = mfacade
				.createMultiscalabilityReport("mreport");
		mfacade.addProjectionValue(mr, projectionvalue);
		facade.addMultiscalabilityReport(report, mr);
		String serialised = facade.serialiseToXML(report);
		ContextReport newReport = facade.unserialiseFromXML(serialised);
		Assert.assertEquals(report.toString(), newReport.toString());
	}

	@Test
	public void testWithJavaScript() throws URISyntaxException, JAXBException,
			ScriptException, ParserConfigurationException, SAXException,
			IOException, XPathExpressionException {
		ContextDataModelFacade facade = new ContextDataModelFacade("facade");
		EntityRelationType typeBelongsTo = facade
				.createEntityRelationType("typeBelongsto");
		ContextEntity sophie = facade.createContextEntity("sophie",
				new URI("mucontext://localhost:3000/sophie"));
		ContextEntity battery = facade.createContextEntity("battery",
				new URI("mucontext://localhost:3000/battery"));
		Vector<ContextEntity> entities = new Vector<ContextEntity>();
		entities.add(battery);
		entities.add(sophie);
		facade.createBinaryEntityRelation("belongsTo", typeBelongsTo, entities);
		ContextObservable lifetime = facade.createContextObservable("lifetime",
				new URI("mucontext://localhost:3000/battery/lifetime"),
				battery);
		ContextObservation<?> observation1 = facade.createContextObservation(
				"observation1", new Double(0.75), lifetime);
		ContextReport report = facade.createContextReport("report");
		facade.addContextObservation(report, observation1);
		String serialised = facade.serialiseToXML(report);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		InputSource is = new InputSource();
		is.setCharacterStream(new StringReader(serialised));
		Document doc = builder.parse(is);
		// create a new JavaScript engine
		ScriptEngine engine = scriptEngineManager.getEngineByName("JavaScript");
		// evaluate the JavaScript code as a new script / function of an
		// object
		addNewFilter(engine);
		Invocable inv = (Invocable) engine;
		// get the MonolithicFilter interface object from scripting
		// engine
		JavaScriptRoutingFilter mf = inv
				.getInterface(JavaScriptRoutingFilter.class);
		// use the script as a filter
		Assert.assertTrue(mf.evaluate(doc));
	}

	@Test
	public void testWithoutFacade() throws URISyntaxException, JAXBException {
		EntityRelationType typeBelongsTo = new EntityRelationType(
				"typeBelongsto");
		ContextEntity sophie = new ContextEntity("sophie",
				new URI("mucontext://localhost:3000/sophie"));
		ContextEntity battery = new ContextEntity("battery",
				new URI("mucontext://localhost:3000/battery"));
		Vector<ContextEntity> entities = new Vector<ContextEntity>();
		entities.add(battery);
		entities.add(sophie);
		EntityRelation belongsTo = new EntityRelation("belongsTo",
				typeBelongsTo, entities);
		battery.addEntityRelation(belongsTo);
		ContextObservable lifetime = new ContextObservable("lifetime",
				new URI("mucontext://localhost:3000/battery/lifetime"),
				battery);
		ContextObservation<Double> observation1 = new ContextObservation<Double>(
				"observation1", new Double(0.75), lifetime, new Date());
		ContextReport report = new ContextReport("report");
		report.addContextObservation(observation1);
		MultiscalabilityDataModelFacade facade = new MultiscalabilityDataModelFacade(
				"facade");
		Viewpoint viewpoint = facade.CreateViewpoint("DEBS");
		Dimension dimension = facade.createDimension("event distribution range",
				viewpoint);
		Measure<?> measure = facade
				.createMeasure("event distribution range measure", false);
		ScaleSet scaleset = facade.createScaleSet(
				"event distribution range"
						+ " in event distribution range measure scaleset",
				dimension, measure);
		Scale scale = facade.createScale("universe", scaleset);
		ProjectionValue projectionvalue = facade.createProjectionValue("pv",
				scale);
		MultiscalabilityReport mr = facade
				.createMultiscalabilityReport("mreport");
		facade.addProjectionValue(mr, projectionvalue);
		report.addMultiscalabilityReport(mr);
		ContextDataModelFacade crFacade = new ContextDataModelFacade("facade");
		crFacade.fileExport(report, "/tmp/mudebs-contex.data");
		ContextReport newReport = crFacade
				.fileImport("/tmp/mudebs-contex.data");
		Assert.assertEquals(report.toString(), newReport.toString());
	}

}

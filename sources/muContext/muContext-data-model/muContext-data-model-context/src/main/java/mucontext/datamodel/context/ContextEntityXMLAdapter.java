/**
This file is part of the INCOME platform.

Copyright (C) 2014-2015 Télécom SudParis

The INCOME platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The INCOME platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the INCOME platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */
package mucontext.datamodel.context;

import java.net.URI;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class ContextEntityXMLAdapter extends XmlAdapter<String, ContextEntity>{

	@Override
	public ContextEntity unmarshal(String v) throws Exception {
		String[] parts = v.split("%%");
		if (parts.length == 2) {
			return new ContextEntity(parts[0], new URI(parts[1]));
		}
		return null;
	}

	@Override
	public String marshal(ContextEntity v) throws Exception {
		String s = v.name + "%%" + v.uri;
		return s;
	}
}

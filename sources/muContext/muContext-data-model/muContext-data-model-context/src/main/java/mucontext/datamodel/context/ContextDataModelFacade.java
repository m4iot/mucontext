/**
This file is part of the INCOME platform.

Copyright (C) 2014-2015 Télécom SudParis

The INCOME platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The INCOME platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the INCOME platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Pierrick MARIE
 */

package mucontext.datamodel.context;

import java.io.File;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URI;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import mucontext.datamodel.multiscalability.EventDistributionRange;
import mucontext.datamodel.multiscalability.MultiscalabilityReport;
import qocim.datamodel.QoCIndicator;

public class ContextDataModelFacade {
	/**
	 * the set of default classes for the JAXB classes.
	 */
	private static Class<?>[] defaultClasses = new Class<?>[] { ContextReport.class, EventDistributionRange.class,
		MultiscalabilityReport.class };
	/**
	 * the name of the facade.
	 */
	private final String name;
	/**
	 * JAXB marshaller for context reports.
	 */
	private Marshaller contextReportMarshaller;

	/**
	 * JAXB unmarshaller for context reports.
	 */
	private Unmarshaller contextReportUnmarshaller;

	public ContextDataModelFacade(final String name) {
		this(name, null);
	}

	public ContextDataModelFacade(final String name, final Class<?>[] otherClasses) {
		this.name = name;
		try {
			Class<?>[] allClasses = null;
			if (otherClasses == null) {
				allClasses = defaultClasses;
			} else {
				allClasses = new Class<?>[defaultClasses.length + otherClasses.length];
				int i;
				for (i = 0; i < defaultClasses.length; i++) {
					allClasses[i] = defaultClasses[i];
				}
				int j;
				for (j = 0; j < otherClasses.length; j++) {
					allClasses[i + j] = otherClasses[j];
				}
			}
			JAXBContext jaxbContext = JAXBContext.newInstance(allClasses);
			jaxbContext = JAXBContext.newInstance(allClasses);
			contextReportMarshaller = jaxbContext.createMarshaller();
			contextReportMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			contextReportMarshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			contextReportUnmarshaller = jaxbContext.createUnmarshaller();
		} catch (final JAXBException e) {
			System.err.format("JAXBException: %s%n", e);
		}
	}

	public ContextEntity addContextObservables(final ContextEntity entity, final ContextObservable observable) {
		if (entity == null || observable == null) {
			return entity;
		}
		return entity.addContextObservable(observable);
	}

	public ContextEntity addContextObservables(final ContextEntity entity, final List<ContextObservable> observables) {
		if (entity == null || observables == null) {
			return entity;
		}
		return entity.addContextObservables(observables);
	}

	public ContextObservable addContextObservation(final ContextObservable observable,
		final ContextObservation<?> observation) {
		if (observable == null || observation == null) {
			return observable;
		}
		return observable.addContextObservation(observation);
	}

	public ContextReport addContextObservation(final ContextReport report, final ContextObservation<?> observation) {
		if (report == null || observation == null) {
			return report;
		}
		return report.addContextObservation(observation);
	}

	public ContextObservable addContextObservations(final ContextObservable observable,
		final List<ContextObservation<?>> observation) {
		if (observable == null || observation == null) {
			return observable;
		}
		return observable.addContextObservations(observation);
	}

	public ContextReport addContextObservations(final ContextReport report,
		final List<ContextObservation<?>> observations) {
		if (report == null || observations == null) {
			return report;
		}
		return report.addContextObservations(observations);
	}

	public ContextReport addMultiscalabilityReport(final ContextReport report, final MultiscalabilityReport mr) {
		if (report == null || mr == null) {
			return report;
		}
		return report.addMultiscalabilityReport(mr);
	}

	public ContextReport addMultiscalabilityReports(final ContextReport report,
		final List<MultiscalabilityReport> mrs) {
		if (report == null || mrs == null) {
			return report;
		}
		return report.addMultiscalabilityReports(mrs);
	}

	public ContextObservation<?> addQoCIndicators(final ContextObservation<?> observation,
		final List<QoCIndicator> qocIndicators) {
		if (observation == null || qocIndicators == null) {
			return observation;
		}
		return observation.addQoCIndicators(qocIndicators);
	}

	public EntityRelation createBinaryEntityRelation(final String name, final EntityRelationType type,
		final List<ContextEntity> entities) {
		if (name == null | name.equals("") || type == null) {
			return null;
		}
		if (entities.size() != 2) {
			return null;
		}
		final EntityRelation relation = new EntityRelation(name, type, entities);
		for (final ContextEntity entity : entities) {
			entity.addEntityRelation(relation);
		}
		return relation;
	}

	public ContextEntity createContextEntity(final String name, final URI uri) {
		if (name == null || name.equals("") || uri == null) {
			return null;
		}
		return new ContextEntity(name, uri);
	}

	public ContextEntity createContextEntity(final String name, final URI uri,
		final List<ContextObservable> observables) {
		if (name == null || name.equals("") || uri == null || observables == null) {
			return null;
		}
		final ContextEntity entity = new ContextEntity(name, uri);
		entity.addContextObservables(observables);
		return entity;
	}

	public ContextObservable createContextObservable(final String name, final URI uri, final ContextEntity entity) {
		if (name == null || name.equals("") || uri == null || entity == null) {
			return null;
		}
		return new ContextObservable(name, uri, entity);
	}

	public <T> ContextObservation<?> createContextObservation(final String id, final T value,
		final ContextObservable observable) {
		if (id == null || id.equals("") || value == null || observable == null) {
			return null;
		}
		return new ContextObservation<T>(id, value, observable, new Date());
	}

	public ContextReport createContextReport(final String id) {
		if (id == null || id.equals("")) {
			return null;
		}
		return new ContextReport(id);
	}

	public EntityRelationType createEntityRelationType(final String name) {
		if (name == null || name.equals("")) {
			return null;
		}
		return new EntityRelationType(name);
	}

	public EntityRelationType createEntityRelationType(final String name, final int min, final int max) {
		if (name == null || name.equals("")) {
			return null;
		}
		if (min < 2) {
			return null;
		}
		return new EntityRelationType(name, min, max);
	}

	public EntityRelation createQuaternaryEntityRelation(final String name, final EntityRelationType type,
		final List<ContextEntity> entities) {
		if (name == null | name.equals("") || type == null) {
			return null;
		}
		if (entities.size() != 4) {
			return null;
		}
		final EntityRelation relation = new EntityRelation(name, type, entities);
		for (final ContextEntity entity : entities) {
			entity.addEntityRelation(relation);
		}
		return relation;
	}

	public EntityRelation createTernaryEntityRelation(final String name, final EntityRelationType type,
		final List<ContextEntity> entities) {
		if (name == null | name.equals("") || type == null) {
			return null;
		}
		if (entities.size() != 3) {
			return null;
		}
		final EntityRelation relation = new EntityRelation(name, type, entities);
		for (final ContextEntity entity : entities) {
			entity.addEntityRelation(relation);
		}
		return relation;
	}

	public final boolean fileExport(final ContextReport report, final String filePath) {
		Boolean retValue = false;
		try {
			final File file = new File(filePath);
			contextReportMarshaller.marshal(report, file);
			retValue = true;
		} catch (final JAXBException e) {
			System.err.format("JAXBException: %s%n", e);
		}
		return retValue;
	}

	public ContextReport fileImport(final String filePath) {
		ContextReport result = null;
		try {
			final File file = new File(filePath);
			result = (ContextReport) contextReportUnmarshaller.unmarshal(file);
		} catch (final JAXBException e) {
			System.err.format("JAXBException: %s%n", e);
		}
		return result;
	}

	public String getName() {
		return name;
	}

	public String serialiseToXML(final ContextReport report) {
		try {
			final java.io.StringWriter sw = new StringWriter();
			contextReportMarshaller.marshal(report, sw);
			return sw.toString();
		} catch (final JAXBException e) {
			System.err.format("JAXBException: %s%n", e);
		}
		return null;
	}

	public final boolean systemOutExport(final ContextReport report) {
		boolean retValue = false;
		try {
			contextReportMarshaller.marshal(report, System.out);
			retValue = true;
		} catch (final JAXBException e) {
			System.err.format("JAXBException: %s%n", e);
		}
		return retValue;
	}

	public ContextReport unserialiseFromXML(final String serialised) {
		ContextReport result = null;
		try {
			result = (ContextReport) contextReportUnmarshaller.unmarshal(new StringReader(serialised));
		} catch (final JAXBException e) {
			System.err.format("JAXBException: %s%n", e);
		}
		return result;
	}
}

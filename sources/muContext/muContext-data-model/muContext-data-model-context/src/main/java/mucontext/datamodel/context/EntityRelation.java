/**
This file is part of the INCOME platform.

Copyright (C) 2014-2015 Télécom SudParis

The INCOME platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The INCOME platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the INCOME platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */

package mucontext.datamodel.context;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

public class EntityRelation {
	public final String name;
	public final EntityRelationType type;
	@XmlJavaTypeAdapter(ContextEntityXMLAdapter.class)
	@XmlElementWrapper(name = "entities")
	@XmlElement(name = "entity")
	public Set<ContextEntity> entities;

	EntityRelation(final String name, final EntityRelationType type,
			final List<ContextEntity> entities) {
		this.name = name;
		this.type = type;
		this.entities = new LinkedHashSet<ContextEntity>();
		if (!(entities == null)) {
			this.entities.addAll(entities);
		}
	}

	EntityRelation() {
		this("", null, null);
	}
	
	public ContextEntity otherEntity(ContextEntity entity) {
		if (entities.size() != 2) {
			return null;
		}
		for (ContextEntity other : entities) {
			if (!entity.equals(other)) {
				return other;
			}
		}
		return null;
	}

	void afterUnmarshal(Unmarshaller u, Object parent) {
		if (parent instanceof ContextEntity) {
			ContextEntity parentEntity = (ContextEntity) parent;
			LinkedHashSet<ContextEntity> newEntities = new LinkedHashSet<ContextEntity>();
			for (ContextEntity entity : entities) {
				if (entity.equals(parent)) {
					newEntities.add(parentEntity);
				} else {
					newEntities.add(entity);
				}
			}
			entities = newEntities;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof EntityRelation)) {
			return false;
		}
		EntityRelation other = (EntityRelation) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

	/**
	 * 
	 * NB: does not include context entities to avoid cycles.
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString(ContextEntity entity) {
		String s = "linking ";
		for (ContextEntity elem : entities) {
			if (elem.equals(entity)) {
				s += entity.name + " ";
			} else {
				s += entity.toString(this) + " ";
			}
		}
		return "EntityRelation [name=" + name + ", type=" + type + ", " + s
				+ "]";
	}
}

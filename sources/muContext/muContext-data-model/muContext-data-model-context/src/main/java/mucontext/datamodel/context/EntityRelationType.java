/**
This file is part of the INCOME platform.

Copyright (C) 2014-2015 Télécom SudParis

The INCOME platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The INCOME platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the INCOME platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */

package mucontext.datamodel.context;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

public class EntityRelationType {
	public final String name;
	public final int minArity;
	public final int maxArity;
	@XmlElementWrapper(name = "relations")
	@XmlElement(name = "relation")
	public Set<EntityRelation> relations;

	EntityRelationType(final String name, final int min, final int max) {
		this.name = name;
		this.minArity = min;
		this.maxArity = max;
		relations = new LinkedHashSet<EntityRelation>();
	}

	EntityRelationType(final String name) {
		this(name, 2, 2);
	}

	EntityRelationType() {
		this("");
	}

	EntityRelationType addEntityRelation(final EntityRelation relation) {
		relations.addAll(relations);
		return this;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof EntityRelationType)) {
			return false;
		}
		EntityRelationType other = (EntityRelationType) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

	/**
	 * 
	 * NB: does not include entity relation to avoid cycles.
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EntityRelationType [name=" + name + ", minArity=" + minArity
				+ ", maxArity=" + maxArity + ", relations=" + relations + "]";
	}
}

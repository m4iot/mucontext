/**
This file is part of the INCOME platform.

Copyright (C) 2014-2015 Télécom SudParis

The INCOME platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The INCOME platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the INCOME platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */

package mucontext.datamodel.context;

import java.net.URI;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlTransient;

public class ContextEntity {
	public final String name;
	public final URI uri;
	@XmlTransient
	public Set<ContextObservable> observables;
	@XmlElementWrapper(name = "relations")
	@XmlElement(name = "relation")
	public Set<EntityRelation> relations;

	ContextEntity(final String name, final URI uri) {
		this.name = name;
		this.uri = uri;
		observables = new LinkedHashSet<ContextObservable>();
		relations = new LinkedHashSet<EntityRelation>();
	}

	ContextEntity() {
		this("", null);
	}

	ContextEntity addContextObservable(ContextObservable observable) {
		if (observable == null) {
			return this;
		}
		observables.add(observable);
		return this;
	}

	ContextEntity addContextObservables(List<ContextObservable> obs) {
		observables.addAll(obs);
		return this;
	}

	ContextEntity addEntityRelation(EntityRelation relation) {
		relations.add(relation);
		return this;
	}

	public EntityRelation relation(String name) {
		for (EntityRelation elem : relations) {
			if (elem.name.equalsIgnoreCase(name)) {
				return elem;
			}
		}
		return null;
	}

	void afterUnmarshal(Unmarshaller u, Object parent) {
		if (parent instanceof ContextObservable) {
			this.observables.add((ContextObservable) parent);
		}
	}

	public String toString(EntityRelation r) {
		String s = "[";
		for (EntityRelation elem : relations) {
			if (elem.equals(r)) {
				s += " " + elem.name;
			} else {
				s += " " + elem.toString(this);
			}
		}
		s += "]";
		return "ContextEntity [name=" + name + ", uri=" + uri + ", relations="
				+ s + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ContextEntity)) {
			return false;
		}
		ContextEntity other = (ContextEntity) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

	/**
	 * 
	 * NB: does not include context observables to avoid cycles.
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String s = "Entity Relations [";
		for (EntityRelation elem : relations) {
			s += " " + elem.toString(this);
		}
		s += "]";
		return "ContextEntity [name=" + name + ", uri=" + uri + ", relations="
				+ s + "]";
	}
}

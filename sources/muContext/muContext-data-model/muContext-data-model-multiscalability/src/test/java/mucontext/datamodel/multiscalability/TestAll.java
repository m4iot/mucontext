/**
This file is part of the muContext middleware.

Copyright (C) 2014-2015 Télécom SudParis

The INCOME platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The INCOME platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the INCOME platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */

package mucontext.datamodel.multiscalability;

import java.net.URISyntaxException;

import javax.script.ScriptEngineManager;
import javax.xml.bind.JAXBException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestAll {

	/**
	 * script engine for JavaScript methods of filters.
	 */
	public ScriptEngineManager scriptEngineManager;

	@Before
	public void setup() throws JAXBException {
		scriptEngineManager = new ScriptEngineManager();
	}

	@Test
	public void testWithoutFacade() throws URISyntaxException, JAXBException {
		Viewpoint viewpoint = new Viewpoint("DEBS");
		Dimension dimension = new Dimension("event distribution range",
				viewpoint);
		Measure<EventDistributionRange> measure = new Measure<EventDistributionRange>(
				"event distribution range measure",
				EventDistributionRange.WORLD);
		ScaleSet scaleset = new ScaleSet(
				"event distribution range in event distribution range measure scaleset",
				dimension, measure);
		Scale scale = new Scale("universe", scaleset);
		ProjectionValue projectionvalue = new ProjectionValue("pv", scale);
		MultiscalabilityReport report = new MultiscalabilityReport("report");
		report.addProjectionValue(projectionvalue);
		MultiscalabilityDataModelFacade facade = new MultiscalabilityDataModelFacade(
				"facade");
		String serialised = facade.serialiseToXML(report);
		MultiscalabilityReport newReport = facade
				.unserialiseFromXML(serialised);
		Assert.assertEquals(report.toString(), newReport.toString());
	}

	@Test
	public void testWithFacade() throws URISyntaxException, JAXBException {
		MultiscalabilityDataModelFacade facade = new MultiscalabilityDataModelFacade(
				"facade");
		Viewpoint viewpoint = facade.CreateViewpoint("DEBS");
		Dimension dimension = facade.createDimension(
				"event distribution range", viewpoint);
		Measure<?> measure = facade.createMeasure(
				"event distribution range measure",
				EventDistributionRange.UNIVERSE);
		ScaleSet scaleset = facade.createScaleSet("event distribution range"
				+ " in event distribution range measure scaleset", dimension,
				measure);
		Scale scale = facade.createScale("universe", scaleset);
		ProjectionValue projectionvalue = facade.createProjectionValue("pv",
				scale);
		MultiscalabilityReport report = facade
				.createMultiscalabilityReport("report");
		facade.addProjectionValue(report, projectionvalue);
		String serialised = facade.serialiseToXML(report);
		MultiscalabilityReport newReport = facade
				.unserialiseFromXML(serialised);
		Assert.assertEquals(report.toString(), newReport.toString());
	}
}

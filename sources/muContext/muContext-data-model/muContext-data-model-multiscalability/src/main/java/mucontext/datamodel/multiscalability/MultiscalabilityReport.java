/**
This file is part of the INCOME platform.

Copyright (C) 2014-2015 Télécom SudParis

The INCOME platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The INCOME platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the INCOME platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */

package mucontext.datamodel.multiscalability;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MultiscalabilityReport {
	public final String id;
	@XmlElementWrapper(name = "projectionvalues")
	@XmlElement(name = "projectionvalue")
	public Set<ProjectionValue> projectionvalues;
	@XmlElementWrapper(name = "reports")
	@XmlElement(name = "report")
	public Set<MultiscalabilityReport> included;

	MultiscalabilityReport(final String id) {
		this.id = id;
		projectionvalues = new LinkedHashSet<ProjectionValue>();
		included = new LinkedHashSet<MultiscalabilityReport>();
	}

	MultiscalabilityReport() {
		this("");
	}

	MultiscalabilityReport addProjectionValue(
			final ProjectionValue projectionvalue) {
		projectionvalues.add(projectionvalue);
		return this;
	}

	MultiscalabilityReport addRecommendations(final List<ProjectionValue> vals) {
		for (ProjectionValue projectionvalue : vals) {
			addProjectionValue(projectionvalue);
		}
		return this;
	}

	MultiscalabilityReport addMultiscalabilityReport(
			final MultiscalabilityReport included) {
		this.included.add(included);
		return this;
	}

	MultiscalabilityReport addMultiscalabilityReports(
			final List<MultiscalabilityReport> inc) {
		this.included.addAll(inc);
		return this;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof MultiscalabilityReport)) {
			return false;
		}
		MultiscalabilityReport other = (MultiscalabilityReport) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "MultiscalabilityReport [id=" + id + ", projectionvalues="
				+ projectionvalues + ", included=" + included + "]";
	}
}

/**
This file is part of the INCOME platform.

Copyright (C) 2014-2015 Télécom SudParis

The INCOME platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The INCOME platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the INCOME platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */

package mucontext.datamodel.multiscalability;

import java.io.File;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import java.util.List;

public class MultiscalabilityDataModelFacade {
	/**
	 * the name of the facade.
	 */
	private String name;
	/**
	 * the set of default classes for the JAXB classes.
	 */
	private static Class<?>[] defaultClasses = new Class<?>[] {
			EventDistributionRange.class, MultiscalabilityReport.class };
	/**
	 * JAXB marshaller for multiscalability reports.
	 */
	private Marshaller multiscalabilityReportMarshaller;

	/**
	 * JAXB unmarshaller for multiscalability reports.
	 */
	private Unmarshaller multiscalabilityReportUnmarshaller;

	public MultiscalabilityDataModelFacade(final String name,
			final Class<?>[] otherClasses) {
		this.name = name;
		try {
			Class<?>[] allClasses = null;
			if (otherClasses == null) {
				allClasses = defaultClasses;
			} else {
				allClasses = new Class<?>[defaultClasses.length
						+ otherClasses.length];
				int i;
				for (i = 0; i < defaultClasses.length; i++) {
					allClasses[i] = defaultClasses[i];
				}
				int j;
				for (j = 0; j < otherClasses.length; j++) {
					allClasses[i + j] = otherClasses[j];
				}
			}
			JAXBContext jaxbContext = JAXBContext.newInstance(allClasses);
			jaxbContext = JAXBContext.newInstance(allClasses);
			multiscalabilityReportMarshaller = jaxbContext.createMarshaller();
			multiscalabilityReportMarshaller.setProperty(
					Marshaller.JAXB_FORMATTED_OUTPUT, true);
			multiscalabilityReportMarshaller.setProperty(
					Marshaller.JAXB_ENCODING, "UTF-8");
			multiscalabilityReportUnmarshaller = jaxbContext
					.createUnmarshaller();
		} catch (JAXBException e) {
			System.err.format("JAXBException: %s%n", e);
		}
	}

	public MultiscalabilityDataModelFacade(final String name) {
		this(name, null);
	}

	public String getName() {
		return name;
	}

	public MultiscalabilityReport createMultiscalabilityReport(final String id) {
		return new MultiscalabilityReport(id);
	}

	public MultiscalabilityReport addProjectionValue(
			final MultiscalabilityReport mr,
			final ProjectionValue projectionvalue) {
		mr.addProjectionValue(projectionvalue);
		return mr;
	}

	public MultiscalabilityReport addProjectionValues(
			final MultiscalabilityReport mr,
			final List<ProjectionValue> projectionvalues) {
		mr.addRecommendations(projectionvalues);
		return mr;
	}

	public MultiscalabilityReport addMultiscalabilityReport(
			final MultiscalabilityReport mr, final MultiscalabilityReport other) {
		mr.addMultiscalabilityReport(other);
		return mr;
	}

	public MultiscalabilityReport addMultiscalabilityReports(
			final MultiscalabilityReport mr,
			final List<MultiscalabilityReport> others) {
		mr.addMultiscalabilityReports(others);
		return mr;
	}

	public Viewpoint CreateViewpoint(final String name) {
		return new Viewpoint(name);
	}

	public Dimension createDimension(final String name,
			final Viewpoint viewpoint) {
		return new Dimension(name, viewpoint);
	}

	public <T> Measure<?> createMeasure(final String name, final T value) {
		return new Measure<T>(name, value);
	}

	public ScaleSet createScaleSet(final String name,
			final Dimension dimension, final Measure<?> measure) {
		return new ScaleSet(name, dimension, measure);
	}

	public Scale createScale(final String name, final ScaleSet scaleset) {
		return new Scale(name, scaleset);
	}

	public ProjectionValue createProjectionValue(final String name,
			final Scale scale) {
		return new ProjectionValue(name, scale);
	}

	public String serialiseToXML(final MultiscalabilityReport report) {
		try {
			java.io.StringWriter sw = new StringWriter();
			multiscalabilityReportMarshaller.marshal(report, sw);
			return sw.toString();
		} catch (JAXBException e) {
			System.err.format("JAXBException: %s%n", e);
		}
		return null;
	}

	public MultiscalabilityReport unserialiseFromXML(final String serialised) {
		MultiscalabilityReport result = null;
		try {
			result = (MultiscalabilityReport) multiscalabilityReportUnmarshaller
					.unmarshal(new StringReader(serialised));
		} catch (JAXBException e) {
			System.err.format("JAXBException: %s%n", e);
		}
		return result;
	}

	public final boolean systemOutExport(final MultiscalabilityReport report) {
		boolean retValue = false;
		try {
			multiscalabilityReportMarshaller.marshal(report, System.out);
			retValue = true;
		} catch (JAXBException e) {
			System.err.format("JAXBException: %s%n", e);
		}
		return retValue;
	}

	public final boolean fileExport(final MultiscalabilityReport report,
			final String filePath) {
		Boolean retValue = false;
		try {
			File file = new File(filePath);
			multiscalabilityReportMarshaller.marshal(report, file);
			retValue = true;
		} catch (JAXBException e) {
			System.err.format("JAXBException: %s%n", e);
		}
		return retValue;
	}

	public MultiscalabilityReport fileImport(final String filePath) {
		MultiscalabilityReport result = null;
		try {
			File file = new File(filePath);
			result = (MultiscalabilityReport) multiscalabilityReportUnmarshaller
					.unmarshal(file);
		} catch (JAXBException e) {
			System.err.format("JAXBException: %s%n", e);
		}
		return result;
	}
}

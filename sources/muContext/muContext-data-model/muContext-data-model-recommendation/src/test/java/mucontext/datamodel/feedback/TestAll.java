/**
This file is part of the muContext middleware.

Copyright (C) 2014-2015 Télécom SudParis

The INCOME platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The INCOME platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the INCOME platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */

package mucontext.datamodel.feedback;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Vector;

import javax.script.ScriptEngineManager;
import javax.xml.bind.JAXBException;

import mucontext.datamodel.context.ContextDataModelFacade;
import mucontext.datamodel.context.ContextEntity;
import mucontext.datamodel.context.ContextObservable;
import mucontext.datamodel.context.ContextObservation;
import mucontext.datamodel.context.ContextReport;
import mucontext.datamodel.context.EntityRelationType;
import mucontext.datamodel.recommendation.Cause;
import mucontext.datamodel.recommendation.Proposition;
import mucontext.datamodel.recommendation.Recommendation;
import mucontext.datamodel.recommendation.RecommendationDataModelFacade;
import mucontext.datamodel.recommendation.RecommendationReport;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestAll {
	/**
	 * script engine for JavaScript methods of filters.
	 */
	public ScriptEngineManager scriptEngineManager;

	@Before
	public void setup() throws JAXBException {
		scriptEngineManager = new ScriptEngineManager();
	}

	@Test
	public void testWithFacade() throws URISyntaxException, JAXBException {
		ContextDataModelFacade facadeContext = new ContextDataModelFacade("facade");
		EntityRelationType typeBelongsTo = facadeContext.createEntityRelationType("typeBelongsto");
		ContextEntity sophie = facadeContext.createContextEntity("sophie", new URI("mucontext://localhost:3000/sophie"));
		ContextEntity battery = facadeContext.createContextEntity("battery", new URI("mucontext://localhost:3000/battery"));
		Vector<ContextEntity> entities = new Vector<ContextEntity>();
		entities.add(battery);
		entities.add(sophie);
		facadeContext.createBinaryEntityRelation("belongsTo", typeBelongsTo, entities);
		ContextObservable lifetime = facadeContext.createContextObservable("lifetime", new URI("mucontext://localhost:3000/battery/lifetime"), battery);
		ContextObservation<?> observation1 = facadeContext.createContextObservation("observation1", new Double(0.75), lifetime);
		ContextReport contextReport = facadeContext.createContextReport("report");
		facadeContext.addContextObservation(contextReport, observation1);
		RecommendationDataModelFacade facadeRecommendation = new RecommendationDataModelFacade("facade");
		Cause cause = facadeRecommendation.createCause("cause1", contextReport);
		Proposition<?> proposition = facadeRecommendation.createProposition("proposition1", "valeur1,valeur2,valeur3");
		Recommendation recommendation = facadeRecommendation.createRecommendation("recommendation1");
		facadeRecommendation.addProposition(recommendation, proposition);
		facadeRecommendation.addCause(recommendation, cause);
		RecommendationReport rr = facadeRecommendation.createRecommendationReport("rr1");
		facadeRecommendation.addRecommendation(rr, recommendation);
		FeedbackDataModelFacade facade = new FeedbackDataModelFacade("facade");
		Feedback feedback = facade.createFeedback("f1", recommendation.propositions, sophie);
		facade.setRecommendationReport(feedback, rr);
		FeedbackReport fr = facade.createFeedbackReport("fr1");
		facade.addFeedback(fr, feedback);
		String serialised = facade.serialiseToXML(fr);
		FeedbackReport newReport = facade.unserialiseFromXML(serialised);
		Assert.assertEquals(fr.toString(), newReport.toString());
	}

	@Test
	public void testWithoutFacade() throws URISyntaxException, JAXBException {
		ContextDataModelFacade facadeContext = new ContextDataModelFacade("facade");
		EntityRelationType typeBelongsTo = facadeContext.createEntityRelationType("typeBelongsto");
		ContextEntity sophie = facadeContext.createContextEntity("sophie", new URI("mucontext://localhost:3000/sophie"));
		ContextEntity battery = facadeContext.createContextEntity("battery", new URI("mucontext://localhost:3000/battery"));
		Vector<ContextEntity> entities = new Vector<ContextEntity>();
		entities.add(battery);
		entities.add(sophie);
		facadeContext.createBinaryEntityRelation("belongsTo", typeBelongsTo, entities);
		ContextObservable lifetime = facadeContext.createContextObservable("lifetime", new URI("mucontext://localhost:3000/battery/lifetime"), battery);
		ContextObservation<?> observation1 = facadeContext.createContextObservation("observation1", new Double(0.75), lifetime);
		ContextReport contextReport = facadeContext.createContextReport("report");
		facadeContext.addContextObservation(contextReport, observation1);
		RecommendationDataModelFacade facadeRecommendation = new RecommendationDataModelFacade("facade");
		Cause cause = facadeRecommendation.createCause("cause1", contextReport);
		Proposition<?> proposition = facadeRecommendation.createProposition("proposition1", "valeur1,valeur2,valeur3");
		Recommendation recommendation = facadeRecommendation.createRecommendation("recommendation1");
		facadeRecommendation.addProposition(recommendation, proposition);
		facadeRecommendation.addCause(recommendation, cause);
		RecommendationReport rr = facadeRecommendation.createRecommendationReport("rr1");
		facadeRecommendation.addRecommendation(rr, recommendation);
		Feedback feedback = new Feedback("f1", recommendation.propositions, sophie);
		feedback.setRecommendationReport(rr);
		FeedbackReport fr = new FeedbackReport("fr1");
		fr.addFeedback(feedback);
		FeedbackDataModelFacade facade = new FeedbackDataModelFacade("facade");
		String serialised = facade.serialiseToXML(fr);
		FeedbackReport newReport = facade.unserialiseFromXML(serialised);
		Assert.assertEquals(fr.toString(), newReport.toString());
	}
}

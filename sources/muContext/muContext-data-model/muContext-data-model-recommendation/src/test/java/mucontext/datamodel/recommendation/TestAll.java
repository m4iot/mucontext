/**
This file is part of the muContext middleware.

Copyright (C) 2014-2015 Télécom SudParis

The INCOME platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The INCOME platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the INCOME platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */

package mucontext.datamodel.recommendation;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Vector;

import javax.script.ScriptEngineManager;
import javax.xml.bind.JAXBException;

import mucontext.datamodel.context.ContextDataModelFacade;
import mucontext.datamodel.context.ContextEntity;
import mucontext.datamodel.context.ContextObservable;
import mucontext.datamodel.context.ContextObservation;
import mucontext.datamodel.context.ContextReport;
import mucontext.datamodel.context.EntityRelationType;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestAll {

	/**
	 * script engine for JavaScript methods of filters.
	 */
	public ScriptEngineManager scriptEngineManager;

	@Before
	public void setup() throws JAXBException {
		scriptEngineManager = new ScriptEngineManager();
	}

	@Test
	public void testWithFacade() throws URISyntaxException, JAXBException {
		ContextDataModelFacade facadeContext = new ContextDataModelFacade("facade");
		EntityRelationType typeBelongsTo = facadeContext.createEntityRelationType("typeBelongsto");
		ContextEntity sophie = facadeContext.createContextEntity("sophie", new URI("mucontext://localhost:3000/sophie"));
		ContextEntity battery = facadeContext.createContextEntity("battery", new URI("mucontext://localhost:3000/battery"));
		Vector<ContextEntity> entities = new Vector<ContextEntity>();
		entities.add(battery);
		entities.add(sophie);
		facadeContext.createBinaryEntityRelation("belongsTo", typeBelongsTo, entities);
		ContextObservable lifetime = facadeContext.createContextObservable("lifetime", new URI("mucontext://localhost:3000/battery/lifetime"), battery);
		ContextObservation<?> observation1 = facadeContext.createContextObservation("observation1", new Double(0.75), lifetime);
		ContextReport contextReport = facadeContext.createContextReport("report");
		facadeContext.addContextObservation(contextReport, observation1);
		RecommendationDataModelFacade facade = new RecommendationDataModelFacade("facade");
		Cause cause = facade.createCause("cause1", contextReport);
		Proposition<?> proposition = facade.createProposition("proposition1", "valeur1,valeur2,valeur3");
		Recommendation recommendation = facade.createRecommendation("recommendation1");
		facade.addProposition(recommendation, proposition);
		facade.addCause(recommendation, cause);
		RecommendationReport report = facade.createRecommendationReport("rr1");
		facade.addRecommendation(report, recommendation);
		String serialised = facade.serialiseToXML(report);
		RecommendationReport newReport = facade.unserialiseFromXML(serialised);
		Assert.assertEquals(report.toString(), newReport.toString());
	}

	@Test
	public void testWithoutFacade() throws URISyntaxException, JAXBException {
		ContextDataModelFacade facadeContext = new ContextDataModelFacade("facade");
		EntityRelationType typeBelongsTo = facadeContext.createEntityRelationType("typeBelongsto");
		ContextEntity sophie = facadeContext.createContextEntity("sophie", new URI("mucontext://localhost:3000/sophie"));
		ContextEntity battery = facadeContext.createContextEntity("battery", new URI("mucontext://localhost:3000/battery"));
		Vector<ContextEntity> entities = new Vector<ContextEntity>();
		entities.add(battery);
		entities.add(sophie);
		facadeContext.createBinaryEntityRelation("belongsTo", typeBelongsTo, entities);
		ContextObservable lifetime = facadeContext.createContextObservable("lifetime", new URI("mucontext://localhost:3000/battery/lifetime"), battery);
		ContextObservation<?> observation1 = facadeContext.createContextObservation("observation1", new Double(0.75), lifetime);
		ContextReport contextReport = facadeContext.createContextReport("report");
		facadeContext.addContextObservation(contextReport, observation1);
		Cause cause = new Cause("cause1", contextReport);
		Proposition<String> proposition = new Proposition<String>("proposition1", "valeur1,valeur2,valeur3");
		Recommendation recommendation = new Recommendation("recommendation1").addProposition(proposition).addCause(cause);
		RecommendationReport report = new RecommendationReport("rr1").addRecommendation(recommendation);
		RecommendationDataModelFacade facade = new RecommendationDataModelFacade("facade");
		facade.fileExport(report, "/tmp/mudebs-recommendation.data");
		RecommendationReport newReport = facade.fileImport("/tmp/mudebs-recommendation.data");
		Assert.assertEquals(report.toString(), newReport.toString());
	}
}


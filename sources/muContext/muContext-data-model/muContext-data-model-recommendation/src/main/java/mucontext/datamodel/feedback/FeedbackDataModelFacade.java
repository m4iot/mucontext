/**
This file is part of the INCOME platform.

Copyright (C) 2014-2015 Télécom SudParis

The INCOME platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The INCOME platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the INCOME platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */

package mucontext.datamodel.feedback;

import java.io.File;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.LinkedHashSet;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import mucontext.datamodel.context.ContextEntity;
import mucontext.datamodel.context.ContextReport;
import mucontext.datamodel.multiscalability.EventDistributionRange;
import mucontext.datamodel.multiscalability.MultiscalabilityReport;
import mucontext.datamodel.recommendation.Proposition;
import mucontext.datamodel.recommendation.RecommendationReport;

public class FeedbackDataModelFacade {
	/**
	 * the name of the facade.
	 */
	private String name;
	/**
	 * the set of default classes for the JAXB classes.
	 */
	private static Class<?>[] defaultClasses = new Class<?>[] {
			ContextReport.class, EventDistributionRange.class,
			RecommendationReport.class, FeedbackReport.class,
			MultiscalabilityReport.class };
	/**
	 * JAXB marshaller for feedback reports.
	 */
	private Marshaller feedbackReportMarshaller;

	/**
	 * JAXB unmarshaller for feedback reports.
	 */
	private Unmarshaller feedbackReportUnmarshaller;

	public FeedbackDataModelFacade(final String name,
			final Class<?>[] otherClasses) {
		this.name = name;
		try {
			Class<?>[] allClasses = null;
			if (otherClasses == null) {
				allClasses = defaultClasses;
			} else {
				allClasses = new Class<?>[defaultClasses.length
						+ otherClasses.length];
				int i;
				for (i = 0; i < defaultClasses.length; i++) {
					allClasses[i] = defaultClasses[i];
				}
				int j;
				for (j = 0; j < otherClasses.length; j++) {
					allClasses[i + j] = otherClasses[j];
				}
			}
			JAXBContext jaxbContext = JAXBContext.newInstance(allClasses);
			jaxbContext = JAXBContext.newInstance(allClasses);
			feedbackReportMarshaller = jaxbContext.createMarshaller();
			feedbackReportMarshaller.setProperty(
					Marshaller.JAXB_FORMATTED_OUTPUT, true);
			feedbackReportMarshaller.setProperty(Marshaller.JAXB_ENCODING,
					"UTF-8");
			feedbackReportUnmarshaller = jaxbContext.createUnmarshaller();
		} catch (JAXBException e) {
			System.err.format("JAXBException: %s%n", e);
		}
	}

	public FeedbackDataModelFacade(final String name) {
		this(name, null);
	}

	public String getName() {
		return name;
	}

	public FeedbackReport createFeedbackReport(final String id) {
		return new FeedbackReport(id);
	}

	public FeedbackReport addFeedback(final FeedbackReport fr,
			final Feedback feedback) {
		fr.addFeedback(feedback);
		return fr;
	}

	public FeedbackReport addFeedbacks(final FeedbackReport fr,
			final List<Feedback> feedbacks) {
		fr.addFeedbacks(feedbacks);
		return fr;
	}

	public FeedbackReport addFeedbackReport(final FeedbackReport fr,
			final FeedbackReport other) {
		fr.addFeedbackReport(other);
		return fr;
	}

	public FeedbackReport addFeedbackReports(final FeedbackReport fr,
			final List<FeedbackReport> others) {
		fr.addFeedbackReports(others);
		return fr;
	}

	public Feedback createFeedback(final String id,
			final Proposition<?> proposition, final ContextEntity entity) {
		LinkedHashSet<Proposition<?>> propositions = new LinkedHashSet<Proposition<?>>();
		propositions.add(proposition);
		return new Feedback(id, propositions, entity);
	}

	public Feedback createFeedback(final String id,
			final LinkedHashSet<Proposition<?>> propositions,
			final ContextEntity entity) {
		return new Feedback(id, propositions, entity);
	}

	public Feedback setRecommendationReport(final Feedback feedback,
			final RecommendationReport rr) {
		return feedback.setRecommendationReport(rr);
	}

	public String serialiseToXML(final FeedbackReport report) {
		try {
			java.io.StringWriter sw = new StringWriter();
			feedbackReportMarshaller.marshal(report, sw);
			return sw.toString();
		} catch (JAXBException e) {
			System.err.format("JAXBException: %s%n", e);
		}
		return null;
	}

	public FeedbackReport unserialiseFromXML(final String serialised) {
		FeedbackReport result = null;
		try {
			result = (FeedbackReport) feedbackReportUnmarshaller
					.unmarshal(new StringReader(serialised));
		} catch (JAXBException e) {
			e.printStackTrace();
			System.err.format("JAXBException: %s%n", e);
		}
		return result;
	}

	public final boolean systemOutExport(final FeedbackReport report) {
		boolean retValue = false;
		try {
			feedbackReportMarshaller.marshal(report, System.out);
			retValue = true;
		} catch (JAXBException e) {
			System.err.format("JAXBException: %s%n", e);
		}
		return retValue;
	}

	public final boolean fileExport(final FeedbackReport report,
			final String filePath) {
		Boolean retValue = false;
		try {
			File file = new File(filePath);
			feedbackReportMarshaller.marshal(report, file);
			retValue = true;
		} catch (JAXBException e) {
			System.err.format("JAXBException: %s%n", e);
		}
		return retValue;
	}

	public FeedbackReport fileImport(final String filePath) {
		FeedbackReport result = null;
		try {
			File file = new File(filePath);
			result = (FeedbackReport) feedbackReportUnmarshaller
					.unmarshal(file);
		} catch (JAXBException e) {
			System.err.format("JAXBException: %s%n", e);
		}
		return result;
	}
}

/**
This file is part of the INCOME platform.

Copyright (C) 2014-2015 Télécom SudParis

The INCOME platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The INCOME platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the INCOME platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */

package mucontext.datamodel.recommendation;

import java.io.File;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import mucontext.datamodel.context.ContextReport;
import mucontext.datamodel.multiscalability.EventDistributionRange;
import mucontext.datamodel.multiscalability.MultiscalabilityReport;

public class RecommendationDataModelFacade {
	/**
	 * the name of the facade.
	 */
	private String name;
	/**
	 * the set of default classes for the JAXB classes.
	 */
	private static Class<?>[] defaultClasses = new Class<?>[] {
			ContextReport.class, EventDistributionRange.class,
			RecommendationReport.class, MultiscalabilityReport.class };
	/**
	 * JAXB marshaller for recommendation reports.
	 */
	private Marshaller recommendationReportMarshaller;

	/**
	 * JAXB unmarshaller for recommendation reports.
	 */
	private Unmarshaller recommendationReportUnmarshaller;

	public RecommendationDataModelFacade(final String name,
			final Class<?>[] otherClasses) {
		this.name = name;
		try {
			Class<?>[] allClasses = null;
			if (otherClasses == null) {
				allClasses = defaultClasses;
			} else {
				allClasses = new Class<?>[defaultClasses.length
						+ otherClasses.length];
				int i;
				for (i = 0; i < defaultClasses.length; i++) {
					allClasses[i] = defaultClasses[i];
				}
				int j;
				for (j = 0; j < otherClasses.length; j++) {
					allClasses[i + j] = otherClasses[j];
				}
			}
			JAXBContext jaxbContext = JAXBContext.newInstance(allClasses);
			jaxbContext = JAXBContext.newInstance(allClasses);
			recommendationReportMarshaller = jaxbContext.createMarshaller();
			recommendationReportMarshaller.setProperty(
					Marshaller.JAXB_FORMATTED_OUTPUT, true);
			recommendationReportMarshaller.setProperty(
					Marshaller.JAXB_ENCODING, "UTF-8");
			recommendationReportUnmarshaller = jaxbContext
					.createUnmarshaller();
		} catch (JAXBException e) {
			System.err.format("JAXBException: %s%n", e);
		}
	}

	public RecommendationDataModelFacade(final String name) {
		this(name, null);
	}

	public String getName() {
		return name;
	}

	public RecommendationReport createRecommendationReport(final String id) {
		return new RecommendationReport(id);
	}

	public RecommendationReport addRecommendation(
			final RecommendationReport rr, final Recommendation recommendation) {
		rr.addRecommendation(recommendation);
		return rr;
	}

	public RecommendationReport addRecommendations(
			final RecommendationReport rr,
			final List<Recommendation> recommendations) {
		rr.addRecommendations(recommendations);
		return rr;
	}

	public RecommendationReport addRecommendationReport(
			final RecommendationReport rr, final RecommendationReport other) {
		rr.addRecommendationReport(other);
		return rr;
	}

	public RecommendationReport addRecommendationReports(
			final RecommendationReport rr,
			final List<RecommendationReport> others) {
		rr.addRecommendationReports(others);
		return rr;
	}

	public Recommendation createRecommendation(final String id) {
		return new Recommendation(id);
	}

	public Recommendation addProposition(final Recommendation recommendation,
			final Proposition<?> proposition) {
		return recommendation.addProposition(proposition);
	}

	public Recommendation addPropositions(final Recommendation recommendation,
			final List<Proposition<?>> propositions) {
		return recommendation.addPropositions(propositions);
	}

	public Recommendation addCause(final Recommendation recommendation,
			final Cause cause) {
		return recommendation.addCause(cause);
	}

	public Recommendation addCauses(final Recommendation recommendation,
			final List<Cause> causes) {
		return recommendation.addCauses(causes);
	}

	public <T> Proposition<?> createProposition(final String name, final T value) {
		return new Proposition<T>(name, value);
	}

	public Cause createCause(final String name, final ContextReport report) {
		return new Cause(name, report);
	}

	public String serialiseToXML(final RecommendationReport report) {
		try {
			java.io.StringWriter sw = new StringWriter();
			recommendationReportMarshaller.marshal(report, sw);
			return sw.toString();
		} catch (JAXBException e) {
			System.err.format("JAXBException: %s%n", e);
		}
		return null;
	}

	public RecommendationReport unserialiseFromXML(final String serialised) {
		RecommendationReport result = null;
		try {
			result = (RecommendationReport) recommendationReportUnmarshaller
					.unmarshal(new StringReader(serialised));
		} catch (JAXBException e) {
			System.err.format("JAXBException: %s%n", e);
		}
		return result;
	}

	public final boolean systemOutExport(final RecommendationReport report) {
		boolean retValue = false;
		try {
			recommendationReportMarshaller.marshal(report, System.out);
			retValue = true;
		} catch (JAXBException e) {
			System.err.format("JAXBException: %s%n", e);
		}
		return retValue;
	}

	public final boolean fileExport(final RecommendationReport report,
			final String filePath) {
		Boolean retValue = false;
		try {
			File file = new File(filePath);
			recommendationReportMarshaller.marshal(report, file);
			retValue = true;
		} catch (JAXBException e) {
			System.err.format("JAXBException: %s%n", e);
		}
		return retValue;
	}

	public RecommendationReport fileImport(final String filePath) {
		RecommendationReport result = null;
		try {
			File file = new File(filePath);
			result = (RecommendationReport) recommendationReportUnmarshaller
					.unmarshal(file);
		} catch (JAXBException e) {
			System.err.format("JAXBException: %s%n", e);
		}
		return result;
	}
}

/**
This file is part of the INCOME platform.

Copyright (C) 2014-2015 Télécom SudParis

The INCOME platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The INCOME platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the INCOME platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */

package mucontext.datamodel.feedback;

import java.util.LinkedHashSet;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import mucontext.datamodel.recommendation.Proposition;

@XmlRootElement
public class FeedbackReport {
	public final String id;
	@XmlElementWrapper(name = "feedbacks")
	@XmlElement(name = "feedback")
	public LinkedHashSet<Feedback> feedbacks;
	@XmlElementWrapper(name = "reports")
	@XmlElement(name = "report")
	public LinkedHashSet<FeedbackReport> included;

	FeedbackReport(final String id) {
		this.id = id;
		feedbacks = new LinkedHashSet<Feedback>();
		included = new LinkedHashSet<FeedbackReport>();
	}

	FeedbackReport() {
		this("");
	}

	FeedbackReport addFeedback(final Feedback feedback) {
		feedbacks.add(feedback);
		return this;
	}

	FeedbackReport addFeedbacks(final List<Feedback> fbcks) {
		for (Feedback feedback : fbcks) {
			addFeedback(feedback);
		}
		return this;
	}

	FeedbackReport addFeedbackReport(
			final FeedbackReport included) {
		this.included.add(included);
		return this;
	}

	FeedbackReport addFeedbackReports(
			final List<FeedbackReport> inc) {
		this.included.addAll(inc);
		return this;
	}

	public LinkedHashSet<Proposition<?>> propositionsMatchFeedback(
			String feedbackName) {
		for (Feedback feedback : feedbacks) {
			if (feedback.name.equalsIgnoreCase(feedbackName)) {
				return feedback.propositions;
			}
		}
		return null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof FeedbackReport)) {
			return false;
		}
		FeedbackReport other = (FeedbackReport) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ContextReport [id=" + id + ", recommendations="
				+ feedbacks + ", included=" + included + "]";
	}
}

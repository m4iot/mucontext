/**
This file is part of the INCOME platform.

Copyright (C) 2014-2015 Télécom SudParis

The INCOME platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The INCOME platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the INCOME platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */

package mucontext.datamodel.recommendation;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

public class Recommendation {
	public final String name;
	@XmlElementWrapper(name = "propositions")
	@XmlElement(name = "proposition")
	public final LinkedHashSet<Proposition<?>> propositions;
	@XmlElementWrapper(name = "causes")
	@XmlElement(name = "cause")
	public final Set<Cause> causes;

	Recommendation(final String id) {
		this.name = id;
		this.propositions = new LinkedHashSet<Proposition<?>>();
		this.causes = new LinkedHashSet<Cause>();
	}

	Recommendation() {
		this("");
	}

	Recommendation addProposition(Proposition<?> proposition) {
		if (propositions != null) {
			propositions.add(proposition);
		}
		return this;
	}

	Recommendation addPropositions(List<Proposition<?>> props) {
		propositions.addAll(props);
		return this;
	}

	Recommendation addCause(Cause cause) {
		if (cause != null) {
			causes.add(cause);
		}
		return this;
	}

	Recommendation addCauses(List<Cause> c) {
		causes.addAll(c);
		return this;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Recommendation)) {
			return false;
		}
		Recommendation other = (Recommendation) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Recommendation [name=" + name + ", propositions="
				+ propositions + ", causes=" + causes + "]";
	}
}

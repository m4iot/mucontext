/**
This file is part of the INCOME platform.

Copyright (C) 2014-2015 Télécom SudParis

The INCOME platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The INCOME platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the INCOME platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */

package mucontext.datamodel.feedback;

import java.util.LinkedHashSet;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import mucontext.datamodel.context.ContextEntity;
import mucontext.datamodel.recommendation.Proposition;
import mucontext.datamodel.recommendation.RecommendationReport;

public class Feedback {
	public final String name;
	@XmlElementWrapper(name = "propositions")
	@XmlElement(name = "proposition")
	public final LinkedHashSet<Proposition<?>> propositions;
	public final ContextEntity choiceMadeBy;
	public RecommendationReport report;

	Feedback(String name, LinkedHashSet<Proposition<?>> propositions,
			ContextEntity entity) {
		this.name = name;
		this.propositions = propositions;
		this.choiceMadeBy = entity;
	}

	Feedback() {
		this("", null, null);
	}

	Feedback setRecommendationReport(RecommendationReport report) {
		this.report = report;
		return this;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Feedback)) {
			return false;
		}
		Feedback other = (Feedback) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Feedback [name=" + name + ", chosenPropositions="
				+ propositions + ", choiceMadeBy=" + choiceMadeBy
				+ ", report=" + report + "]";
	}

}

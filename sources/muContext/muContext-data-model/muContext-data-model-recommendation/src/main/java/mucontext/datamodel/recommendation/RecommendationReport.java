/**
This file is part of the INCOME platform.

Copyright (C) 2014-2015 Télécom SudParis

The INCOME platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The INCOME platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the INCOME platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */

package mucontext.datamodel.recommendation;

import java.util.LinkedHashSet;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RecommendationReport {
	public final String id;
	@XmlElementWrapper(name = "recommendations")
	@XmlElement(name = "recommendation")
	public LinkedHashSet<Recommendation> recommendations;
	@XmlElementWrapper(name = "reports")
	@XmlElement(name = "report")
	public LinkedHashSet<RecommendationReport> included;

	RecommendationReport(final String id) {
		this.id = id;
		recommendations = new LinkedHashSet<Recommendation>();
		included = new LinkedHashSet<RecommendationReport>();
	}

	RecommendationReport() {
		this("");
	}

	RecommendationReport addRecommendation(final Recommendation recommendation) {
		recommendations.add(recommendation);
		return this;
	}

	RecommendationReport addRecommendations(final List<Recommendation> recs) {
		for (Recommendation recommendation : recs) {
			addRecommendation(recommendation);
		}
		return this;
	}

	RecommendationReport addRecommendationReport(
			final RecommendationReport included) {
		this.included.add(included);
		return this;
	}

	RecommendationReport addRecommendationReports(
			final List<RecommendationReport> inc) {
		this.included.addAll(inc);
		return this;
	}

	public LinkedHashSet<Proposition<?>> propositionsMatchRecommendation(
			String recommendationName) {
		for (Recommendation recommendation : recommendations) {
			if (recommendation.name.equalsIgnoreCase(recommendationName)) {
				return recommendation.propositions;
			}
		}
		return null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof RecommendationReport)) {
			return false;
		}
		RecommendationReport other = (RecommendationReport) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ContextReport [id=" + id + ", recommendations="
				+ recommendations + ", included=" + included + "]";
	}
}

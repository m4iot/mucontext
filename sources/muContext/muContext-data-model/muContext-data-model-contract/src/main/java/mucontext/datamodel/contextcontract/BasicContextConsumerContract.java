/**
This file is part of the muContext middleware.

Copyright (C) 2014-2017 Télécom SudParis

The INCOME platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The INCOME platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the INCOME platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */

package mucontext.datamodel.contextcontract;

import mucontext.api.ContextConsumerContract;
import mudebs.common.algorithms.OperationalMode;
import mudebs.common.algorithms.routing.ABACInformation;

/**
 * This class defines a basic context consumer contract composed of an
 * identifier, a routing filter, and ABAC information.
 * 
 * @author Denis Conan
 */
public class BasicContextConsumerContract extends BasicContextContract
		implements ContextConsumerContract {
	/**
	 * the ABAC information of the context contract.
	 */
	ABACInformation abacInformation;

	/**
	 * constructor.
	 * 
	 * @param id
	 *            the identifier of the contract.
	 * @param m
	 *            the operational mode of the calls: global or local.
	 * @param f
	 *            the JavaScript function of the filter.
	 * @param abac
	 *            the ABAC information for matching the XACML policy of the
	 *            producer.
	 */
	public BasicContextConsumerContract(final String id,
			final OperationalMode m, final String f,
			final ABACInformation abac) {
		super(id, m, f);
		abacInformation = abac;
	}

	/**
	 * gets the ABAC information of the context contract.
	 * 
	 * @return the ABAC information.
	 */
	public ABACInformation abacInformation() {
		return abacInformation;
	}

	/*
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BasicContextConsumerContract [abacInformation="
				+ abacInformation + ", identifier=" + identifier + ", local="
				+ mode + ", routingFilterScript=" + routingFilterScript + "]";
	}
}

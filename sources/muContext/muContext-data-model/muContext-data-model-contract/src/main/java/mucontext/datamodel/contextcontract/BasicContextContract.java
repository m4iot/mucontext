/**
This file is part of the muContext middleware.

Copyright (C) 2014-2015 Télécom SudParis

The INCOME platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The INCOME platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the INCOME platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */

package mucontext.datamodel.contextcontract;

import mucontext.api.ContextContract;
import mudebs.common.algorithms.OperationalMode;
import mudebs.common.algorithms.routing.MultiScopingSpecification;

/**
 * This class defines a basic context contract composed of an identifier and a
 * routing filter.
 * 
 * @author Denis Conan
 * @author Léon Lim
 */
public class BasicContextContract implements ContextContract {
	/**
	 * the identifier of the context contract.
	 */
	String identifier;
	/**
	 * the operational mode of the contract (local or global).
	 */
	OperationalMode mode;
	/**
	 * the set of sets of scope paths associated with the filter.
	 */
	MultiScopingSpecification phi;
	/**
	 * the routing filter of the context contract.
	 */
	String routingFilterScript;

	/**
	 * constructor.
	 * 
	 * @param id
	 *            the identifier of the contract.
	 * @param mode
	 *            the operational mode of the calls: global or local.
	 * @param phi
	 *            the multiscoping information of the contract.
	 * @param filter
	 *            the JavaScript function of the filter.
	 */
	public BasicContextContract(final String id, final OperationalMode mode,
			final MultiScopingSpecification phi, final String filter) {
		if (id == null || id.equals("")) {
			throw new IllegalArgumentException("wrong identifier");
		}
		identifier = id;
		this.mode = mode;
		this.phi = phi;
		if (filter == null || filter.equals("")) {
			throw new IllegalArgumentException("wrong JavaScript filter");
		}
		routingFilterScript = filter;
	}

	/**
	 * constructor.
	 * 
	 * @param id
	 *            the identifier of the contract.
	 * @param mode
	 *            the operational mode of the calls: global or local.
	 * @param filter
	 *            the JavaScript function of the filter.
	 */
	public BasicContextContract(final String id, final OperationalMode mode,
			final String filter) {
		this(id, mode, null, filter);
	}

	/**
	 * gets the identifier of the context contract.
	 */
	public String identifier() {
		return identifier;
	}

	/**
	 * gets the boolean stating whether the subscription is local.
	 */
	public OperationalMode operationalMode() {
		return mode;
	}

	/**
	 * gets the script of muDEBS routing filter of the context contract.
	 */
	public String routingFilterScript() {
		return routingFilterScript;
	}

	/**
	 * gets the multi-scoping specification, that is the set of scopes. This set
	 * can be <tt>null</tt>.
	 */
	public MultiScopingSpecification mulscopingSpecification() {
		return phi;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((identifier == null) ? 0 : identifier.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof BasicContextContract)) {
			return false;
		}
		BasicContextContract other = (BasicContextContract) obj;
		if (identifier == null) {
			if (other.identifier != null) {
				return false;
			}
		} else if (!identifier.equals(other.identifier)) {
			return false;
		}
		return true;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BasicContextContract [identifier=" + identifier + ", local="
				+ mode + ", routingFilterScript=" + routingFilterScript
				+ ", ssp=" + phi + "]";
	}
}

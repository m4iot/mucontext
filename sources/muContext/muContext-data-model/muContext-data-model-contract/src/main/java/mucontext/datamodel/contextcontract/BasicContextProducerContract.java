/**
This file is part of the muContext middleware.

Copyright (C) 2014-2017 Télécom SudParis

The INCOME platform is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The INCOME platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the INCOME platform.  If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s):
 */

package mucontext.datamodel.contextcontract;

import mucontext.api.ContextProducerContract;
import mudebs.common.algorithms.OperationalMode;
import mudebs.common.filters.XACMLPolicy;

/**
 * This class defines a basic context consumer contract composed of an
 * identifier, a routing filter, and XACML policy.
 * 
 * @author Denis Conan
 */
public class BasicContextProducerContract extends BasicContextContract
		implements ContextProducerContract {
	/**
	 * the XACML policy of the context contract.
	 */
	XACMLPolicy policy;

	/**
	 * constructor.
	 * 
	 * @param id
	 *            the identifier of the contract.
	 * @param m
	 *            the operational mode of the calls: global, local.
	 * @param f
	 *            the JavaScript function of the filter.
	 * @param p
	 *            the XACML policy of the contract.
	 */
	public BasicContextProducerContract(final String id,
			final OperationalMode m, final String f, final XACMLPolicy p) {
		super(id, m, f);
		policy = p;
	}

	/**
	 * gets the XACML policy.
	 * 
	 * @return the XACML policy.
	 */
	public XACMLPolicy policy() {
		return policy;
	}

	/*
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BasicContextProducerContract [policy=" + policy
				+ ", identifier=" + identifier + ", local=" + mode
				+ ", routingFilterScript=" + routingFilterScript + "]";
	}
}
